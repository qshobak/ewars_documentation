setup:
	pip install -r requirements.txt;
	(cd theme_src && pip install -r requirements.txt && npm install);
	(cd revealplugin && python setup.py install);

rebuild_plugin:
	(cd revealplugin && python setup.py install);

.PHONY: setup rebuild
