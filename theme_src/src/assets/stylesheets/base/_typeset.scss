////
/// Copyright (c) 2016-2018 Martin Donath <martin.donath@squidfunk.com>
///
/// Permission is hereby granted, free of charge, to any person obtaining a
/// copy of this software and associated documentation files (the "Software"),
/// to deal in the Software without restriction, including without limitation
/// the rights to use, copy, modify, merge, publish, distribute, sublicense,
/// and/or sell copies of the Software, and to permit persons to whom the
/// Software is furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
/// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
/// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
/// DEALINGS
////

// ----------------------------------------------------------------------------
// Rules: font definitions
// ----------------------------------------------------------------------------
$dt-header-height: 107px;

// Enable font-smoothing in Webkit and FF
body {
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}

// Default fonts
body,
input {
  color: $clr-bright-gray;
  font-feature-settings: "kern", "liga";
  font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
}

// Proportionally spaced fonts
pre,
code,
kbd {
  color: $clr-bright-gray;
  font-feature-settings: "kern";
  font-family: "Courier New", Courier, monospace;
}

// ----------------------------------------------------------------------------
// Rules: typesetted content
// ----------------------------------------------------------------------------

// Content that is typeset - if possible, all margins, paddings and font sizes
// should be set in ems, so nested blocks (e.g. Admonition) render correctly,
// except headlines that should only appear on the top level and need to have
// consistent spacing due to layout constraints.
.md-typeset {
  font-size: ms(-1);
  line-height: 1.6;

  // Colors should be kept when printing
  -webkit-print-color-adjust: exact;

  // Default spacing
  p,
  ul,
  ol,
  blockquote {
    margin: 1em 0;
  }

  // 1st level headline
  h1 {
    margin: 0 0 4rem;
    color: $clr-theme-a;
    font-size: ms(3);
    font-weight: 700;
    letter-spacing: -0.01em;
    line-height: 1.3;

    .theme-a & {
      color: $clr-theme-a;
    }

    .theme-b & {
      color: $clr-theme-b;
    }

    .theme-c & {
      color: $clr-theme-c;
    }

    .theme-d & {
      color: $clr-theme-d;
    }
  }

  // 2nd level headline
  h2 {
    margin: 4rem 0 1.6rem;
    font-size: ms(2);
    font-weight: 700;
    letter-spacing: -0.01em;
    line-height: 1.4;
  }

  // 3rd level headline / made bold
  h3 {
    margin: 3.2rem 0 1.6rem;
    font-size: ms(1);
    font-weight: bold;
    letter-spacing: -0.01em;
    line-height: 1.5;
  }

  // 3rd level headline following an 2nd level headline
  h2+h3 {
    margin-top: 1.6rem;
  }

  // 4th level headline
  h4 {
    margin: 1.6rem 0;
    font-size: ms(0);
    font-weight: 700;
    letter-spacing: -0.01em;
  }

  // 5th and 6th level headline
  h5,
  h6 {
    margin: 1.6rem 0;
    color: $md-color-black--light;
    font-size: ms(-1);
    font-weight: 700;
    letter-spacing: -0.01em;
  }

  // Overrides for 5th level headline
  h5 {
    text-transform: uppercase;
  }

  // Horizontal separators
  hr {
    margin: 1.5em 0;
    border-bottom: 0.1rem dotted $md-color-black--lighter;
  }

  // Links
  a {
    color: $md-color-primary;
    word-break: break-word;

    // Also enable color transition on pseudo elements
    &,
    &::before {
      transition: color 0.125s;
    }

    // Hovered and active links
    &:hover,
    &:active {
      color: $md-color-accent;
    }

    // Add URLs for print
    @media print {

      // Show link URL
      &::after {
        color: $md-color-black--light;
        content: " ["attr(href) "]";
      }
    }
  }

  // Code blocks - increase 85% - 95%, make bold
  code,
  pre {
    background-color: $md-code-background;
    color: $md-code-color;
    font-size: 95%;
    direction: ltr;
	font-weight: bold;
	font-family: Roboto;
	  
    // Wrap text and hide scollbars
    @media print {
      white-space: pre-wrap;
    }
  }

  // Inline code blocks, correct relative ems for smaller font size - increase padding from 0.0625 to 0.0825 all round
  code {
    $correct: 1 / 0.85;

    margin: 0 0.25em * $correct;
    padding: 0.0825em * $correct;
    border-radius: 0.2rem;
    box-shadow:
      +0.25em * $correct 0 0 $md-code-background,
      -0.25em * $correct 0 0 $md-code-background;
    word-break: break-word;
    box-decoration-break: clone;

    // Remove box-shadows for print
    @media print {
      box-shadow: none;
      box-decoration-break: initial;
    }
  }

  // Disable containing block inside headlines
  h1 code,
  h2 code,
  h3 code,
  h4 code,
  h5 code,
  h6 code {
    margin: 0;
    background-color: transparent;
    box-shadow: none;
  }

  // Reset code if it's inside a link
  a>code {
    margin: inherit;
    padding: inherit;
    border-radius: none;
    background-color: inherit;
    color: inherit;
    box-shadow: none;
  }

  // Unformatted code blocks
  pre {
    position: relative;
    margin: 1em 0;
    border-radius: 0.2rem;
    line-height: 1.4;
    -webkit-overflow-scrolling: touch;

    // [mobile -]: Stretch to whole width
    @include break-to-device(mobile) {
      margin: 1em -1.6rem;
      border-radius: 0;
    }

    // Actual container with code, overflowing
    >code {
      display: block;
      margin: 0;
      padding: 1.05rem 1.2rem;
      background-color: transparent;
      font-size: inherit;
      box-shadow: none;
      box-decoration-break: none;
      overflow: auto;

      // [mobile -]: Increase padding to match text
      @include break-to-device(mobile) {
        padding: 1.05rem 1.6rem;
      }

      // Override native scrollbar styles
      &::-webkit-scrollbar {
        width: 0.4rem;
        height: 0.4rem;
      }

      // Style scrollbar thumb
      &::-webkit-scrollbar-thumb {
        background-color: $md-color-black--lighter;

        // Hovered scrollbar thumb
        &:hover {
          background-color: $md-color-accent;
        }
      }
    }
  }

  // Keystrokes
  kbd {
    $correct: 1 / 0.85;

    padding: 0 0.25em * $correct;
    border: 0.1rem solid darken($md-keyboard-background, 20%);
    border-radius: 0.3rem;
    border-bottom-color: darken($md-keyboard-background, 25%);
    background-color: $md-keyboard-background;
    color: $md-keyboard-color;
    font-size: 85%;
    box-shadow: 0 0.1rem 0 darken($md-keyboard-background, 30%);
    word-break: break-word;
  }

  // Text highlighting marker
  mark {
    margin: 0 0.25em;
    padding: 0.0625em 0;
    border-radius: 0.2rem;
    background-color: transparentize($clr-yellow-500, 0.5);
    box-shadow:
      +0.25em 0 0 transparentize($clr-yellow-500, 0.5),
      -0.25em 0 0 transparentize($clr-yellow-500, 0.5);
    word-break: break-word;
    box-decoration-break: clone;
  }

  // Abbreviations
  abbr {
    border-bottom: 0.1rem dotted $md-color-black--light;
    text-decoration: none;
    cursor: help;
  }

  // Small text
  small {
    opacity: 0.75;
  }

  // Superscript and subscript
  sup,
  sub {
    margin-left: 0.0625em * 1 / 0.8;

    // Adjust for RTL languages
    [dir="rtl"] & {
      margin-right: 0.0625em * 1 / 0.8;
      margin-left: initial;
    }
  }

  // Blockquotes, possibly nested
  blockquote {
    padding-left: 1.2rem;
    border-left: 0.4rem solid $md-color-black--lighter;
    color: $md-color-black--light;

    // Adjust for RTL languages
    [dir="rtl"] & {
      padding-right: 1.2rem;
      padding-left: initial;
      border-right: 0.4rem solid $md-color-black--lighter;
      border-left: initial;
    }
  }

  // Unordered lists
  ul {
    list-style-type: disc;
  }

  // Unordered and ordered lists
  ul,
  ol {
    margin-left: 0.625em;
    padding: 0;

    // Adjust for RTL languages
    [dir="rtl"] & {
      margin-right: 0.625em;
      margin-left: initial;
    }

    // Nested ordered lists
    ol {
      list-style-type: lower-alpha;

      // Triply nested ordered list
      ol {
        list-style-type: lower-roman;
      }
    }

    // List elements
    li {
      margin-bottom: 0.5em;
      margin-left: 1.25em;

      // Adjust for RTL languages
      [dir="rtl"] & {
        margin-right: 1.25em;
        margin-left: initial;
      }

      // Decrease vertical spacing
      p,
      blockquote {
        margin: 0.5em 0;
      }

      // Remove margin on last element
      &:last-child {
        margin-bottom: 0;
      }

      // Nested lists
      ul,
      ol {
        margin: 0.5em 0 0.5em 0.625em;

        // Adjust for RTL languages
        [dir="rtl"] & {
          margin-right: 0.625em;
          margin-left: initial;
        }
      }
    }
  }

  // Definition lists
  dd {
    margin: 1em 0 1em 1.875em;

    // Adjust for RTL languages
    [dir="rtl"] & {
      margin-right: 1.875em;
      margin-left: initial;
    }
  }

  // Limit width to container
  iframe,
  img,
  svg {
    max-width: 100%;
  }

  // Data tables - changed font-size to ms(-1)
	// remove 0.2rem border radius
  table:not([class]) {
    @include z-depth(2);

    display: inline-block;
    max-width: 100%;
    border-radius: 0;
    font-size: ms(-1);
    overflow: auto;
    -webkit-overflow-scrolling: touch;

    // Due to margin collapse because of the necessary inline-block hack, we
    // cannot increase the bottom margin on the table, so we just increase the
    // top margin on the following element
    &+* {
      margin-top: 1.5em;
    }

    // Table headings and cells
    th:not([align]),
    td:not([align]) {
      text-align: left;

      // Adjust for RTL languages
      [dir="rtl"] & {
        text-align: right;
      }
    }

    // Table headings
    th {
      min-width: 10rem;
      padding: 1.2rem 1.6rem;
      // background-color: $md-color-black--light;
      color: $md-color-white;
      vertical-align: top;

      .theme-a & {
        background-color: $clr-theme-a;
      }
  
      .theme-b & {
        background-color: $clr-theme-b;
      }
  
      .theme-c & {
        background-color: $clr-theme-c;
      }
  
      .theme-d & {
        background-color: $clr-theme-d;
      }
    }
    // Table cells
    td {
      padding: 1.2rem 1.6rem;
      border-top: 0.1rem solid $md-color-black--lightest;
      vertical-align: top;
    }
    tr:nth-child(even) td {
      /*.theme-a & {
        background-color: transparentize($clr-theme-a, 0.92);
      }
  
      .theme-b & {
        background-color: transparentize($clr-theme-b, 0.92);
      }
  
      .theme-c & {
        background-color: transparentize($clr-theme-c, 0.92);
      }
  
      .theme-d & {
        background-color: transparentize($clr-theme-d, 0.92);
      }*/
      .theme-a, .theme-b, .theme-c, .theme-d {
		    background: rgba(246, 246, 246, 1);
      }
    }
    // Remove top border on first row
    tr:first-child td {
      border-top: 0;
    }

    // Do not wrap links in tables
    a {
      word-break: normal;
    }
  }

  // Wrapper for scrolling on overflow
  &__scrollwrap {
    margin: 1em -1.6rem;
    overflow-x: auto;
    -webkit-overflow-scrolling: touch;
  }

  // Data table wrapper, in case JavaScript is available
  .md-typeset__table {
    display: inline-block;
    margin-bottom: 0.5em;
    padding: 0 1.6rem;

    // Data tables
    table {
      display: table;
      width: 100%;
      margin: 0;
      overflow: hidden;
    }
  }

	/*Resize from
	width: 300px;
    height: 288px; 
	*/
  .card {
    width: 250px;
    height: 200px;
    border-radius: 4px;
    padding: 8px;
    margin: 4px;
    &:last-child {
      margin-right:  0;
    }
    &:first-child {
      margin-left:  0;
    }
  }
  .card-icon {
    width: 80px;
    height: 80px;
    font-size: 18px;
    text-align: center;
    line-height: 80px;
    color: white;
    margin: 24px auto 0;
    border-radius: 50%;
  }
	/* resize from
	 height: 58px;
	 font-size: 18px;
	*/
  .card__title {
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 16px;
    color: #3f424f;
    font-weight: bold;
    width: 220px;
    margin: 5px auto;
    text-align: center;
	height: 40px;
  }
  .card__btn {
    width: 96px;
    height: 26px;
    line-height: 26px;
    border-radius: 2px;
    background-color: #ffffff;
    font-size: 12px;
    font-weight: 400;
    padding: 0 8px;
    margin: -2px auto 0;
  }
  .home-page {
    display: flex;
    flex-wrap: wrap;
    margin: 0 -5px;
    justify-content: center;
    @media screen and (min-width: 660px) {
      justify-content: flex-start;
    }
    h1 {
      margin: 0;
    }
  }
  .theme-a & .card, & .card.card--t-a, & .card-row.card--t-a {
    background-color: transparentize($color: $clr-theme-a, $amount: 0.92);
    .card-icon {
      background-color: $clr-theme-a;
    }
    .card__btn {
      color: $clr-theme-a;
      a {
        color: $clr-theme-a;
      }
    }
  }
  .theme-b & .card, & .card.card--t-b, & .card-row.card--t-b {
    background-color: transparentize($color: $clr-theme-b, $amount: 0.92);
    .card-icon {
      background-color: $clr-theme-b;
    }
    .card__btn {
      color: $clr-theme-b;
      a {
        color: $clr-theme-b;
      }
    }
  }
  .theme-c & .card, & .card.card--t-c, & .card-row.card--t-c {
    background-color: transparentize($color: $clr-theme-c, $amount: 0.92);
    .card-icon {
      background-color: $clr-theme-c;
    }
    .card__btn {
      color: $clr-theme-c;
      a {
        color: $clr-theme-c;
      }
    }
  }
  .theme-d & .card, & .card.card--t-d, & .card-row.card--t-d {
    background-color: transparentize($color: $clr-theme-d, $amount: 0.92);
    .card-icon {
      background-color: $clr-theme-d;
    }
    .card__btn {
      color: $clr-theme-d;
      a {
        color: $clr-theme-d;
      }
    }
  }
  .card.card--t-e {
    background-color: #DFE3EC;
    .card-icon {
    background-color: grey;
    }
    .card__btn {
      color: $clr-theme-d;
      a {
        color: $clr-theme-d;
      }
    }
  }	
		
  p.card__p {
    text-align: center;
    color: #3f424f;
    font-size: 15px;
  }
  
  .card__tall {
    height: 300px !important;
    width: 288px !important;
  }
    
  .card__home {
    height: 280px;
    width: 280px;
    border-radius: 12px;
    border: 1px solid #f0f3f5;
    background-color: white!important;

    p.card__p {
      font-size: 14px;
    }
    .card-icon {
      border-radius: 12px;
      height: 111px;
      line-height: 111px;
      font-size: 50px;
      font-weight: 500;
    }
    .card__title {
      font-size: 18px;
    }
    &:hover {
      &.card--t-a {
        border-color: $clr-theme-a;
      }
      &.card--t-b {
        border-color: $clr-theme-b;
      }
      &.card--t-c {
        border-color: $clr-theme-c;
      }
      &.card--t-d {
        border-color: $clr-theme-d;
      }
    }
  }
  // download pdf links in guidance pages 
  .guid-pdf {
 	float: right;
    width: 110px;
    height: 26px;
    line-height: 26px;
    border-radius: 2px;
    background-color: #eeee;
    font-size: 14px;
    font-weight: 400;
    margin: 10px auto 0;
    padding: 0 8px;
  }
  .guid-pdf-link {
 	color: #3f424f !important;
  }
}

.d-lg-none {
  display: block;
  @media screen and (min-width: 1219px) {
    display: none;
  }
}
