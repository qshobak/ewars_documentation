# EWARS documentation

This documentation repo uses `mkdocs`, this needs to be installed before the documetnation can be built or tested.

You can install `mkdocs` on macos with the command 

`pip install mkdocs`

To run a local development server while writing out the documentation:

`mkdocs serve`

To deploy the documentation:

`make deploy` which will deploy the static site the S3 bucket (if you have the correctly configured AWS credentials in your system).

Loads at: http://docs.staging.ewars.ws/

## Site build

To build the doc site itself locally, make sure that you have the following pre-requisites set up:

You can also do the below by cd'ing into the directory and running `make setup` from the command-line

1. Python 2.7 or later
2. Install the reveal plugin by running `(cd revealplugin && python setup.py install)`
2. mkdocs `pip install mkdocs` or `pip install -r requirements.txt` in the root directory

Once you have the pre-requisites you can run a development server of the site by issuing the command `mkdocs serve` in the same directory as `mkdocs.yml`

## Theme build

Firstly, **DO NOT EDIT/DELETE** or otherwise modify anything in the `theme_src/material` folder. This is the build folder for the theme and any changes you make within the folder will be overwritten when the next build is triggered.

Counter to normaly process where the build artefacts are not included in source control. When you are issuing a pull request to Chris's master branch on the main repo, run the build and commit the build artefacts as part of your pull request. This ensures that Chris or anyone else working on the actual documentation has a working version of the theme in master without having to set up the dependencies and run the build himself.

In order to setup development of the theme located in the `theme_src` folder you will need to install some pre-requisites. 

### Build setup

1. Ensure you have python2.7+ installed on your system as well a relatively recent nodejs/npm install
2. change directory into the theme_src folder
3. `pip install -r requirements.txt`
4. `npm install`

This should set up everything for the builds.

### Testing

`npm run watch` will spawn a webpack build and rebuild on any changes in the theme_src folder. 

### Build

Run `npm run build` to have the final theme built into the `material` folder

### Template engine

The template engine used in the theme is Jinja2, if you are not familiar with it's syntax you can find it here [Jinja1](http://jinja.pocoo.org/) it's a fairly robust and common python templating language that's the default for almost every python framework out there including Django, Flask, Tornado, Pyramid, Pylons, Bottle, etc...

### Custom styling in guidance

**Technical guidance**

| syntax | colour | icon | 
| -- | -- | -- |
| !!! checklist | green | check mark |
| !!! guidance | blue | help |
| !!! references | grey | bookmark |
| !!! case-study | light red | location pin |
| !!! ewars | purple | ewars app |

**User guidance**

| syntax | colour | icon | 
| -- | -- | -- |
| !!! tip | green | flare |
| !!! warning | orange | exclamation mark |
| !!! danger | red | exclamation mark |
| !!! admin | dark grey | cog |

**Customised**

| syntax | colour | icon | 
| -- | -- | -- |
| !!! case-box | light red | no icon |
| !!! definition | blue | no icon |
| !!! more-details | grey | no icon |

### Custom styling in presentations


**Customised**

| syntax | class | colour | icon | 
| -- | -- | -- | -- |
| - Checklist | <!-- .element: class="success" | green | check mark
| - Definition | <!-- .element: class="note" | blue | edit icon |
| - Case Study | <!-- .element: class="case-study" | red | no icon |
| - More Details | <!-- .element: class="more-details" | blue | no icon |
| - Reference | <!-- .element: class="reference" | grey | no icon |
