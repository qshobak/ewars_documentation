import os
import sys
from functools import partial

import pkg_resources

from mkdocs.plugins import BasePlugin

def build_docs(tmpl, source, target):
    if not os.path.exists(target):
        os.makedirs(target)

    for entry in os.listdir(source):
        if entry.endswith(".md"):
            data = None
            entry_path = os.path.join(source, entry)
            with open(entry_path, "r") as f:
                data = f.read()

            if data is not None:
                result = tmpl.replace("{CONTENT}", data)
                out = os.path.join(target, entry.split(os.sep)[-1].replace(".md", ".html"))
                with open(out, "w") as f:
                    f.write(result)


class RevealPlugin(BasePlugin):

    def on_serve(self, server, config):
        tmpl = None
        tmpl_path = pkg_resources.resource_filename("revealplugin", "data/template.html")

        with open(tmpl_path, "r") as f:
            tmpl = f.read()

        site_dir = config.get("site_dir")
        output_path = os.path.join(site_dir, "presentations")
        docs_dir = config.get("docs_dir").split(os.sep)
        sep = str(os.sep)
        res_dir = ""
        for item in docs_dir[0:-1]:
            res_dir = res_dir + "%s%s" % (item, os.sep,)

        source_dir = os.path.join(res_dir, "presentations")

        part = partial(build_docs, tmpl, source_dir, output_path)
        server.watch(source_dir, part)

        return server

    def on_post_build(self, config):
        tmpl = None
        tmpl_path = pkg_resources.resource_filename("revealplugin", "data/template.html")

        with open(tmpl_path, "r") as f:
            tmpl = f.read()

        site_dir = config.get("site_dir")
        output_path = os.path.join(site_dir, "presentations")
        docs_dir = config.get("docs_dir").split(os.sep)
        sep = str(os.sep)
        res_dir = ""
        for item in docs_dir[0:-1]:
            res_dir = res_dir + "%s%s" % (item, os.sep,)

        source_dir = os.path.join(res_dir, "presentations")

        build_docs(tmpl, source_dir, output_path)
