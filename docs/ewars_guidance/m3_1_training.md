# Training

### Sorry, this module isn't ready yet!

<!-- # Training

## 1. Training strategy

EWAR must have exhaustive coverage among the population; consider that disease events could emerge both from crowded urban areas with high potential for exposure to rural and remote areas with less access to health care. Therefore, training in detection should address all levels of skill and access to the health care system which may include:

- Community representatives (i.e., teachers, religious leaders, etc.);
- Community health workers, community health volunteers, and hygiene promoters;
- Health care workers at the primary health care level.

It is often difficult to assemble health facility staff in one workshop given their workload and the effects of their collective absence on care at the health facilities. Multiple phases of training with district health authorities and health facilities are necessary to cover relevant staff, for example:

- Phase 1: Workshop with NGO health coordinators and district-level health authorities
- Phase 2: Multiple workshops or site visits for health facility focal points/other reporting staff
- Phase 3: Mass training of community health workers, community leaders, etc.

## 2. Training materials
Soon after, specific training packages and job-aids can be adapted/developed:

- Master training plan and training materials
- Job-aids appropriate for the literacy level of health workers (vetted by health workers)
- Posters to introduce the program to the community (for health facilities, health service points, etc.) -->


