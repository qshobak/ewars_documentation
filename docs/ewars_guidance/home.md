# EWAR in emergencies

<div class="home-page" style="width: 95%">
    <h1></h1>
    <div class="card card--t-e">
        <a href="/ewars_guidance/m0_0_about.html">
            <div class="card-icon"></div>
            <h2 class="card__title">About</h2>
        </a>
        <div class="card__btn">
            <a href="/ewars_guidance/m0_0_about.pdf" target="__blank">Download <strong>PDF</strong></a>
        </div>
    </div>
    <div class="card card--t-e">
        <a href="/ewars_guidance/m0_1_introduction.html">
            <div class="card-icon"></div>
            <h2 class="card__title">Introduction</h2>
        </a>
        <div class="card__btn">
            <a href="/ewars_guidance/m0_1_introduction.pdf" target="__blank">Download <strong>PDF</strong></a>
        </div>
    </div>
    <div class="card card--t-a">
        <a href="/ewars_guidance/m1_1_objectives.html">
            <div class="card-icon">1</div>
            <h2 class="card__title">Define objectives</h2>
        </a>
        <div class="card__btn">
            <a href="/ewars_guidance/m1_1_objectives.pdf" target="__blank">Download PDF</a>
        </div>
    </div>
    <div class="card card--t-a">
        <a href="/ewars_guidance/m1_2_team.html">
            <div class="card-icon">2</div>
            <h2 class="card__title">Form team</h2>
        </a>
        <div class="card__btn">
            <a href="/ewars_guidance/m1_2_team.pdf" target="__blank">Download <strong>PDF</strong></a>
        </div>
    </div>
    <div class="card card--t-a">
        <a href="/ewars_guidance/m1_3_assess.html">
            <div class="card-icon">3</div>
            <h2 class="card__title">Assess national capacity</h2>
        </a>
        <div class="card__btn">
            <a href="/ewars_guidance/m1_3_assess.pdf" target="__blank">Download <strong>PDF</strong></a>
        </div>
    </div>
    <div class="card card--t-a">
        <a href="/ewars_guidance/m2_1_ibs.html">
            <div class="card-icon">4</div>
            <h2 class="card__title">Establish IBS</h2>
        </a>
        <div class="card__btn">
            <a href="/ewars_guidance/m2_1_ibs.pdf" target="__blank">Download <strong>PDF</strong></a>
        </div>
    </div>
    <div class="card card--t-a">
        <a href="/ewars_guidance/m2_2_ebs.html">
            <div class="card-icon">5</div>
            <h2 class="card__title">Establish EBS</h2>
        </a>
        <div class="card__btn">
            <a href="/ewars_guidance/m2_2_ebs.pdf" target="__blank">Download <strong>PDF</strong></a>
        </div>
    </div>
    <div class="card card--t-a">
        <a href="/ewars_guidance/m2_3_alert.html">
            <div class="card-icon">6</div>
            <h2 class="card__title">Establish alert management</h2>
        </a>
        <div class="card__btn">
            <a href="/ewars_guidance/m2_3_alert.pdf" target="__blank">Download <strong>PDF</strong></a>
        </div>
    </div>
    <div class="card card--t-a">
        <a href="/ewars_guidance/m2_4_response.html">
            <div class="card-icon">7</div>
            <h2 class="card__title">Prepare to respond</h2>
        </a>
        <div class="card__btn">
            <a href="/ewars_guidance/m2_4_response.pdf" target="__blank">Download <strong>PDF</strong></a>
        </div>
    </div>
    <div class="card card--t-a">
        <a href="/ewars_guidance/m3_1_training.html">
            <div class="card-icon">8</div>
            <h2 class="card__title">Conduct training</h2>
        </a>
        <div class="card__btn">
            <a href="/ewars_guidance/m3_1_training.pdf" target="__blank">Download <strong>PDF</strong></a>
        </div>
    </div>
    <div class="card card--t-a">
        <a href="/ewars_guidance/m3_2_implement.html">
            <div class="card-icon">9</div>
            <h2 class="card__title">Implement, supervise and monitor</h2>
        </a>
        <div class="card__btn">
            <a href="/ewars_guidance/m3_1_training.pdf" target="__blank">Download <strong>PDF</strong></a>
        </div>
    </div>
    <div class="card card--t-a">
        <a href="/ewars_guidance/m3_3_analysis.html">
            <div class="card-icon">10</div>
            <h2 class="card__title">Provide analysis and feedback</h2>
        </a>
        <div class="card__btn">
            <a href="/ewars_guidance/m3_3_analysis.pdf" target="__blank">Download <strong>PDF</strong></a>
        </div>
    </div>
    <div class="card card--t-a">
        <a href="/ewars_guidance/m3_4_evaluate.html">
            <div class="card-icon">11</div>
            <h2 class="card__title">Evaluate performance</h2>
        </a>
        <div class="card__btn">
            <a href="/ewars_guidance/m3_4_evaluate.pdf" target="__blank">Download <strong>PDF</strong></a>
        </div>
    </div>
    <div class="card card--t-a">
        <a href="/ewars_guidance/m3_5_exit.html">
            <div class="card-icon">12</div>
            <h2 class="card__title">Prepare exit strategy</h2>
        </a>
        <div class="card__btn">
            <a href="/ewars_guidance/m3_5_exit.pdf" target="__blank">Download <strong>PDF</strong></a>
        </div>
    </div>
</div>