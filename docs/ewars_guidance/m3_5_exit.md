# Exit strategy

### Sorry, this module isn't ready yet!

<!-- The EWAR system may remain functional years after the initial acute emergency for which it was intended. 

- The exit or transition strategy should be carried out to ensure that it does not replace the development of the national surveillance system in the emergency-affected area, or undermine national capacity. 

- At the same time, the objectives of the EWAR system should be integrated into the national surveillance system in order to meet IHR obligations.

The exit or transition strategy should address:

- Integration of the EWAR framework into surveillance and response;
- Ongoing development of local capacity for early detection, investigation, verification, and response;
- Transition of key infrastructure, key roles and the workflow, to the national surveillance system. -->
