 <div class="guid-pdf"><a class="guid-pdf-link" href="/ewars_guidance/m2_3_alert.pdf" target="__blank">Download <strong>PDF</strong></a></div>

# Alert management

!!! checklist "Checklist"
		1. Create alert log
		2. Define alert management workflow
		3. Define verification procedures
		4. Standardise risk assessment
		5. Standardise risk characterisation
		6. Standardise outcome
		7. Form rapid response teams

!!! ewars "See how on EWARS-in-a-box supports alert management [on a mobile phone](../ewars_application/mobile/app_mobile_alert.html) or [on a laptop](../ewars_application/desktop/app_desktop_alert.html)"

___


## 1. Create alert log

### What are alerts?

!!! definition ""

	**Alerts** are the first hints of a larger public health problem. They represent the first pieces of sparse data or information that hint at a potential risk to health.

- Alerts come from either IBS and EBS sources. 

- All alerts detected by an EWAR system, regardless of the source, should be systematically documented in a central **alert log** and managed in the same, predictable manner. 

- This helps to ensure that all alerts are recorded and acted upon consistently, and that the process can be monitored.

___

### What should the alert log record?

!!! more-details "The alert log should record:"

	- the nature of the alerts (e.g. date, type, location) and 
	- the actions taken (investigation, verification, response).

- Alert logs should be kept at both national and subnational level and should be reviewed daily for outstanding actions. 

- Summary information on the numbers of alerts triggered, and the actions taken, should be sent weekly to the next level in the system and reported in the Epidemiological Bulletin.

- The steps to manage each alert are described below. 

- All alerts should be verified, but only a small proportion will go on to require risk assessment. And even fewer events will be considered high or very high risk, and require large-scale response (see Figure)

**Figure**		Proportional number of alerts requiring follow-up at each step

![Alert circles](../assets/guidance/alert_circles.svg "Alert circles")

___

## 2. Standardise alert management workflow

!!! more-details ""

	All alerts must be managed according to a standard workflow that consists of the following steps:

	1. Verification
	2. Risk Assessment
	3. Risk Characterisation
	4. Risk Outcome

- Each step should be conducted as close as possible to the field-level. 

- Staff at higher levels in the system should be able to access information on the alerts and monitor performance, but should only become directly involved if additional expertise or support is required.

!!! more-details "Why manage alerts at field level?"

	- Decentralizing alert management helps to ensure the rapid follow-up and processing of all alerts. 

	- Field-level staff are able to more quickly find source of the alert, and can better understand the epidemiological and socio-cultural context in which it occured. This is essential to be able to ask the right questions and to obtain accurate information.

	- In emergencies, this means that county or district level officers may need to be trained to conduct the initial verification of alerts. If verified, this can then be escalated to the next level in the system to determine if additional support is needed to conduct a risk assessment.

**Fig 1** Alert management workflow

| Stage | Responsibility | Performance |
| --- | --- | --- |
| Verification | County or District level | Within 48 hours of an alert being triggered |
| Risk assessment  | County or District level and/or higher level* | Within 48 hours of an alert being verified |
| Risk characterisation | By the risk assessment team | At the time of the risk assessment |
| Outcome | By the risk assessment team | At the time of the risk assessment |

!!! more-details
	Note: Higher levels (such as Province or State) should support risk assessment of high threat alerts. These should be clearer indicated in Alert SOPs. 

___ 

## 3. Define verification procedures

### How are alerts verified to be events?

!!! definition ""
	- **Verification** describes the process used to determine whether or not an initial alert is valid and requires escalation to the next level of risk assessment. 
	- It should remain as quick and straightforward a process as possible.

- It is important to develop a structured process and set of questions to verify alerts.

- Depending on the setting, verification can include different means of collecting information (e.g. by telephone or through field visit). 

- All alerts must be verified as quickly as possible and at least within 48 hours of detection. 

- This requires staff at subnational levels to be trained and involved in process. 

- It also requires stronger implementation and use of electronic tools that can provide access to a common alert log to key staff at sub-national levels.

___


### Key information to support verification of an alert

- The information submitted with an alert will depend on the information collected in the IBS and EBS forms in a given emergency. 

- As a guiding principle it should include a number of required fields:

!!! more-details "Key information to support verification of an alert"

	**Source:**

	- Name of reporter
	- Contact information of the reporter (e.g. mobile number)
	- Original source of information (if second-hand)

	**Location:**

	- Location of alert (e.g. house address, village, nearest landmark)

	**Description:**

	- Date of onset of symptoms
	- Symptoms and signs of cases (to consider differential diagnoses)
	- Age distribution of cases (< 5 years / > 5 years)
	- Outcomes of cases (cases / deaths)
	- Actions taken

___


### Key questions to determine validity of an alert

- Based on the information submitted, some key questions can help to detemine the validity of an alert.

!!! more-details "Questions to help determine the validity of an alert"

	**Source** 

	- Has the event been reported by an official source (e.g. local health-care centre or clinic, public health authorities, animal health workers)

	**Frequency** 

	- Has the event been reported by multiple independent sources (e.g. residents, news media, health-care workers)?

	**Epidemiology** 

	- Does the event description include details about time, place and people involved (e.g. six people are sick and two died three days after a ending a local celebration on in community X)?

	**Clinical details** 

	- Is the clinical presentation of the cases described (e.g. a cluster of seven people admitted to hospital with atypical pneumonia, of whom two have died)?

	**Consistency** 

	- Has a similar event been reported previously (e.g. with a similar presentation, affecting a similar population and geographical area, over the same me period)?

___

### Outcomes of verification

- At the end of the verification stage, there are a number of possible actions that can be taken.

!!! more-details "Possible actions at the verification stage:"
	1. **Discard** as that the alert is:

	  	- Verified to be false rumor
	  	- Verified to refer to a disease or hazard that is not amenable to immediate public health action (e.g., alert of several diabetes cases, varicella cases, etc.)

	2. **Monitor** as there is still not enough information for an alert to be verified (e.g. the reporter cannot be found). This means that the alert remains at the verification stage, pending a definitive outcome to either discard it or escalate it to risk assessment.

	3. **Risk asssess** as the alert is verified as genuine, in which case it is classified as a real public health event. This means that is must be risk-assessed to understand the potential impact it may have on public health.


___


## 4. Conduct a risk assessment 

### When is it done?

!!! definition ""
	**Events** are verified alerts that warrant risk assessment.

- Risk assessment should be carried out within 48 hours of an alert being verified as an event.

- The level of risk will change over time, meaning that risk assessment is a systematic and continuous process. 

- It should be done immediately after the alert is verified in the field, and then may need to be repeated at a later stage as soon as new information becomes available (e.g. new cases, new areas affected, changes in outbreak indicators).

___

### How is it done?

- Risk assessment is a dynamic process that may change in complexity based on the needs of any given event. 

- For EWAR, the purpose is to rapidly characterize the probability that a verified alert will have a serious public health impact, and to help determine the actions needed to reduce this risk.

!!! more-details "The level of risk is based the following categories:"

	1. Hazard 
	2. Exposure 
	3. Context 

![Alert Venn](../assets/guidance/risk_venn.svg "Alert Venn")


- The information collected under each category will assist in determining whether the event meets the criteria for response.

**Table** Key information collected in risk assessment

| **Components** | **Key information** | 
| --- | --- | 
| **Hazard assessment** <br>Identification of the hazard (i.e. cholera), the characteristics of a public health hazard and health effects.   |- Laboratory confirmation<br>- Otherwise, listing of possible causes based on clinical and epidemiological features<br> |
| **Exposure assessment** <br>Evaluation of the exposure of individuals and populations to likely hazards.<br>|- # of people likely exposed<br>- # exposed likely susceptible<br>- Vaccination coverage|
| **Context assessment** <br>Evaluation of the context which may affect either the transmission potential or overall impact of the event | - Environment (e.g. climate, vegetation, land use)<br>- Health and nutritional status<br>- Cultural practices and beliefs<br>- Infrastructure (access, services)<br>- Social context (e.g. ongoing civil war, refugee camp) |

___

### Who is responsible?

- Standard operating procedures should define which types of alerts will require which level of support for risk assessment. 

- Depending on the context, district or county level staff may have the capacity to risk assess certain types of alerts. For other alerts, additional technical resources or expertise may be required from higher levels in the system.

- More detailed risk assessments are needed when events appear particularly dangerous for public health.

- A team with appropriate level of specialization should be assembled to carry out a risk assessment.

___

### Role of laboratory surveillance 

- Laboratories play an essential role in supporting EWAR by:

	- **Generating alerts**. Rapid diagnostic tests sometimes play a role in generating alerts (e.g. positive cholera rapid diagnostic test among one or more suspect cases)

	- **Helping to characterise a hazard**. Laboratory confirmation often plays a pivotal role in the characterisation of a hazard during a risk assessment or intial public health investigation. 

- Support for laboratory testing and confirmation should be achieved as rapidly as possible, ideally at peripheral levels of the health system, to support characterisation of the level of risk and identification of the control measures required. 

___

## 5. Standardise risk characterisation

- !!! definition ""
	**Risk characterisation** is the assignment of a level of risk to an event, according to its likelihood of occuring and the resulting public health consequences.

- For some events, the information is limited and/or the overall level of risk is obvious and can be characterized automatically. 

- However, a useful tool is a **risk matrix** where estimates of the likelihood are combined with estimates of the consequences.

![Risk Matrix](../assets/guidance/risk_matrix.svg "Risk Matrix")

- !!! more-details "The two key questions to ask when assigning a level of risk are:""
	- What is the likelihood of further spread?
	- What would be the consequences (type and magnitude) to public health if this were to occur?

___


### Levels of risk

- The level of risk in turn helps to determine the proportional level of resources that are required to mount a commensurate response.

!!! more-details "Risk levels"
	<table style="width: 100%; box-shadow: none; border: 0px">
		<tr style="background-color: #eeeeee">
			<td style="width: 25%;background-color: #eeeeee"">![Low Risk](../assets/guidance/risk-low.svg "Low Risk")</td>
			<td style="background-color: #eeeeee">Managed in accordance with standard intervention protocols, routine control programs and regulations (eg, surveillance using routine surveillance systems)</td>
		</tr>
		<tr style="background-color: #eeeeee">
			<td style="width: 25%;background-color: #eeeeee"">![Moderate Risk](../assets/guidance/risk-mod.svg "Moderate Risk")</td>
			<td style="background-color: #eeeeee">The roles and responsibility of the response must be specified. Specific surveillance or control measures required (eg enhanced surveillance, supplementary vaccination campaigns)</td>
		</tr>
		<tr style="background-color: #eeeeee">
			<td style="width: 25%;background-color: #eeeeee"">![High Risk](../assets/guidance/risk-high.svg "High Risk")</td>
			<td style="background-color: #eeeeee">Special attention from management is needed: it may be necessary to put in place command and control structures; a series of additional control measures will be needed, some of which may have significant consequences</td>
		</tr>
		<tr style="background-color: #eeeeee">
			<td style="width: 25%;background-color: #eeeeee"">![Very High Risk](../assets/guidance/risk-vhigh.svg "Very High Risk")</td>
			<td style="background-color: #eeeeee">An immediate response is required even if the event is reported outside normal business hours. Immediate attention from the required senior management (for example, the command and control structure should be put in place within a few hours); the implementation of control measures with serious consequences is very likely</td>
		</tr>
	</table>

___

### How long does it take?

- The process should not be overly complicated or lengthy. 

- It should be guided by expert opinion of the risk assessment team, with attempts to reach consensus based on the evidence that is available.

- During discussions, team members should consider all types of consequences of the event in addition to the expected magnitude (e.g. morbidity and mortality).

- The risk matrix also helps to assess and document changes in risk as new information becomes available, and before and after control measures are implemented.

___

## 6. Standardise risk outcome

- The final step is a decision to assign a final risk outcome to the event, based on the results of the risk assessment and risk characterisation.


!!! more-details "Possible actions at the outcome stage:"

	1. **Discard** as following risk assessment you determine is not amenable to immediate public health action (e.g., alert of several diabetes cases, varicella cases, etc.)

	2. **Monitor** as there is still not enough information for an alert to be verified (e.g. the reporter cannot be found). This means that the alert remains at the verification stage, pending a definitive outcome to either discard it or escalate it to risk assessment.

	3. **Respond** based on the level of risk and the set of pre-defined actions that you have determined using the risk matrix. 

!!! guidance "See [Module 7: Be ready to respond](m2_4_response.html) for more information on the next step, and the response measures that EWAR should be prepared to support."  
___


## 7. Form rapid response teams

- To ensure a predictable early response to alerts that are triggered through IBS or EBS, a rapid response team (RRT) should be formed at each level of the EWAR system to support the risk assessment of alerts. 

- The role of the RRTs is to conduct risk assessments of verified alerts, and to support additional public health investigations.

- The composition of a team will depend on the nature of the threat being risk assessed. This can commonly include:

|Function|Role|
|--|--|
| Epidemiology|Provide understanding on infectious disease threats, sources and modes of transmission|
| Laboratories and health services|Support specimen collection and transport|
| Clinicians | Support case management, IPC, immunization|
| Veterinarians|For zoonotic threats where indicated|
| Information and communication| Support communication and sharing of the results, with media, stakeholders and communities|

___

### Standard operating procedures

- It is important to have SOPs to guide how the RRTs will work.

!!! more-details "These should clearly define:"
	- Who is a member of the RRT at each level of the system 
	- Which alerts will be responded to at which level 
	- The mechanisms for how an RRT can request support from higher levels

___

### References 

!!! references
	1. Rapid risk assessment of acute public health events. Geneva, World Health Organization, 2012.
	1. Early detection, assessment and response to acute public health events: implementation of early warning and response with a focus on event-based surveillance. Geneva, World Health Organization, 2014.
