 <div class="guid-pdf"><a class="guid-pdf-link" href="/ewars_guidance/m2_2_ebs.pdf" target="__blank">Download <strong>PDF</strong></a></div>

# Event-based Surveillance

## EBS Checklist 

!!! checklist

	1. Agree on strategy
	2. Select priority diseases and other hazards
	3. Define event case definitions
	4. Define event thresholds
	5. Strengthen data collection and reporting

!!! ewars "See how on EWARS-in-a-box supports event-based surveillance [on a mobile phone](../ewars_application/mobile/app_mobile_reporting.html) or [on a laptop](../ewars_application/desktop/app_desktop_reporting.html)"

___ 

## The basics of EBS 

!!! definition ""
	**Event-based surveillance (EBS)** is the organized collection, monitoring, analysis and interpretation of mainly _unstructured ad hoc information_ regarding health events or risks, which may represent an acute risk to human health. 

- EBS has the potential to provide real-time detection of any acute public health event, particularly those that are not well detected by indicator-based surveillance (IBS)

- There is no regular reporting frequency for EBS. Reports should be submitted anytime, as soon as events occur.

- EBS can use multiple sources, from both official and unofficial sources and across multiple sectors (including animal health). 

___

### Key characteristics

**Table 1** Updated Key characteristics of EBS

|Characteristic|Description|
|--|--|
| **Key strength**| To trigger real-time alerts of disease outbreaks and other public health events not reflected by IBS case definitions | 
| **Data sources** | - Community members<br>- Community health workers/volunteers<br>- Healthcare facilities<br>- Media<br>- Telephone hotlines (Public)|
| **Characteristics** |- May have less structured reporting format<br>- Often includes non-infectious hazards<br>- May have a less formal case definitions<br>- May have less formal alert thresholds |
| **Process** |- Can use both structured and unstructured networks<br>- Ad-hoc frequency as events are detected<br>- Emphasis on real-time triggering of alerts |

___


## Implementation steps

### 1. Agree on strategy


- An important use of EBS in emergencies is to detect events at community-level that might otherwise go undetected by IBS (or only be detected very late).

- EBS must remain flexible enough to collect unstructured information from multiple data sources, including both official and unofficial sources.

___

#### Choice of data sources

- The EBS strategy must identify the types of events to be put under surveillance, and assess the sources available to report this informtation.

- These sources can also include non-health sources, including media and animal health.

- EBS should aim to strengthen detection of events before presentation to health care; at community-based health care; and by health workers in health facilities.

___

#### Community-based surveillance

!!! definition ""
	**Community based surveillance (CBS)** is the systematic detection and reporting of events of public health significance within a community, by community members.

- Based on existing networks of community health workers, Red Cross/Red Crescent (RC) Movement volunteers, and other volunteer networks based within communities, there is an increased potential to detect initial clusters of disease and suspected cases before or during an established outbreak in rural and remote communities.

- Such community-based surveillance (CBS) networks supplement routine health-facility based surveillance in order to provide a more structured communication mechanism between community members and public health authorities. 


###### Role of CBS in EWAR

- In emergencies, CBS is most commonly used to support EBS. However, it can also support IBS when organised networks of community health workers and volunteers are available.

!!! more-details

	IFRC and WHO highlight four settings where CBS can be used to support the early warning function of an EWAR system:

	- **During an outbreak** to widen the reporting network coverage, monitor trends, and inform the response (e.g. plague in Madagascar; use of real-time ORP-based reporting of cholera by CHWs);
	
	- **During emergencies** where the routine surveillance system is non-functional, to monitor high-risk, epidemic prone diseases as a more structured, community-based supplement to an EWAR (e.g. HEV in South Sudan);
	
	- In **complex environments**, to fill gaps where routine surveillance is not functional, has poor coverage, and where community-based reporting provides the only surveillance information (e.g. malaria in Central African Republic)
	
	- In **communities lacking social cohesion**, i.e., large and urban communities or those divided among ethno-linguistic differences

- CBS volunteers can signal events related to disease transmission (e.g. clusters of cases of a similar, unusual set of symptoms) or simplified case definitions (e.g. rash and fever as suspected measles). 

###### Planning consideratons for CBS

- Engaging communities in CBS is not straightforward and requires pre-planning to be effective. 

- Risks and considerations for operational implementation and sustainability are given in the table below.

**Table** Planning considerations for community-based surveillance

| **Theme** |         **Risk**             |      **Considerations**     |
| --------- | ---------------------------- | --------------------------- |
| Planning |- Poor community acceptance<br>- Poor vigilance by CHW/RCs<br>- Poor coverage|- Does the community want to input into the system? What are the potential negative consequences for CHWs and communities in reporting suspected diseases/outbreaks?<br>- Is the CHW/RC network is able to effectively handle the additional workload on top of other activities (i.e., iCCM, malnutrition screening, health promotion, AFP, etc)?<br>- Is the CHW/RC network distributed amongst all rural and remote areas of risk?<br>- What incentives are needed?|
| Human resources |- Poor CHW/RC technical capacity<br>- Mis-specification of events<br>- Overburdening CHW|- What is the most effective and sustainable community network to use?<br>- RC volunteers (who have basic health training)<br>- iCCM CHWs (who have basic health training)<br>- CHVs doing malnutrition screening (no health training)<br>- Are data collection tools are appropriate for the level of literacy and numeracy?|
| Sustainability |- Waning effectiveness<br>- Unmet community expectations<br>- Expenses outstrip funding<br>- Logistics management and operational costs|- What are the incentives, either monetary or in-kind (i.e., clothing designating affiliation with the program) and is this sustainable?<br>- Can training, supervision, and monitoring be ensured past the acute phase, in order, to keep adequate levels of quality in reporting and vigilance?<br>- Is sustainability beyond the acute emergency desired? If so, how will incentives and supervision be funded?|
| Specificity and verification system |- System overwhelmed<br>- Low contribution of relevant alerts to EWAR |- Balancing sensitivity and specificity of CBS as simple and broad signal definitions make CBS more sensitive but less specific<br> - Is there a reliable verification for the many community alerts which will come in? Normally, this requires a separate filtering step and staff, and is not handled by the EWAR itself.<br>- Is the system set up to be as specific as possible? A system that is too sensitive (i.e., due to too many reportable events, event descriptions which are too vague, poor training, etc) will risk a high number of false positives. |
| Analysis, and response |- System overwhelmed|- Is there a reliable response system?<br>- Does this involve the CHW network or does it feed into the EWAR response system?|

___


## 2. Select priority diseases and other hazards

!!! more-details ""
	EBS will be overwhelmed if it tries to cover all diseases and hazards captured in IBS. 

- It is recommended to first proceed through the IBS disease and conditions selection process, and then focus on the gaps which can be addressed by EBS.

- The list of diseases and conditions targeted by EBS should be regularly reviewed, to reflect the epidemiological context and any emerging hazards.


___


#### Categories to consider

- EBS should focus on diseases and conditions not well captured through IBS. 

- This includes:

	- **Emerging or re-emerging infectious diseases** not yet known to an area or no longer prevalent

	- **Outbreaks of infectious disease at community-level**, that are not yet large enough to be detected through IBS in health facilities

	- **Non-infectious hazards** (e.g. chemical and environmental hazards) that are not well-covered by case definitions of national surveillance.

___

#### Criteria to guide selection

!!! more-details "Criteria to guide selection of priority diseases and conditions"

	The same criteria presented in IBS can also be used to guide the selection under EBS. _In addition_ the following criteria should be applied:

	- What are the current major public health hazards that are currently not well detected by IBS? (e.g. clusters of unusual or unknown disease at community-level)

	- Which diseases could emerge or re-emerge that are currently under surveillance in IBS?
	
	- What non-infectious hazards could present risk in this context? (e.g. chemical or environmental causes)
	
___

## 3. Define event case definitions
 
- An important use of EBS in emergencies is to detect events at community-level, that might otherwise go undetected (or only be detected very late) by the network of health facilities implementing IBS.

- Where EBS is used to supplement the surveillance of epidemic-prone diseases under IBS, it can be important to provide health workers at both community and health-facility levels with simple definitions of events to look out for. 

Table	Events appropriate for healthcare workers

| Alert | Information to provide in comments box of weekly report form |
|--|--|
| ARI | Have you noted any clusters of severe acute respiratory illness in the same block/neighbourhood?· Have you seen an unexpected increase in the number of deaths from severe acute respiratory illness/pneumonia? |
| AWD | Have any cases had severe dehydration requiring hospitalisation?· Have there been any deaths?· Were there any cases from the host community? |
| Bloody diarrhoea | Have cases required hospitalisation?·  Have there been any deaths?·  Were any cases clustered in the same block? If yes, how many? |
| Unexplained fever | Have you noticed an increase in severe cases with fever or deaths?·  Are you suspecting any specific clinical condition that may explain this increase? If so, please provide further details. |


___

## 4. Define event alert thresholds

- There are no predefined thresholds used to define when alerts are triggered in EBS (compared to the use of alert thresholds in IBS). 

- Instead, the submission of an EBS report on a certain disease or public health hazard in itself should trigger an alert and should lead to the standardised set of actions to manage the alert in the same was as alerts triggered through IBS.

___

### Management of EBS alerts

- Once an alert has been triggered in EBS, it should be managed according to a predefined workflow and set of standard operating procedures. These are discussed in more detail in the alert management module.

!!! guidance "See [Module 6: Setup alert management](m2_3_alert.html) for how EBS alerts should now be managed."  

___

### 5. Strengthen data collection and reporting

#### Data collection 

- Data on priority events should be reported using a standard EBS form. 

- The EBS reporting form may differ according to source. All should include basic information on the person, place and time of the alert.

!!! more-details  "Standard variables for an EBS reporting form"
	- Source of report
	- Location
	- Nature of the hazard (infectious, chemical, radio-nuclear, etc.)
	- Date of event 
	- Date of onset of symptoms
	- Number of case(s)/death(s) 
	- Number of people potentially exposed 


___

#### Frequency of reporting

- EBS reports should be reported **immediately** to facilitate real-time detection and response to EBS alerts. 

- EBS generally produces more sensitive data to detect events as early as possible. This often happens at the expense of generating a high proportion of false positive reports. 

- Weekly or monthly reporting is too infrequent for the purpose of immediate detection and verification of alerts.

___


#### Supervision and feedback

- Data collection and reporting should be strengthened through regular monitoring and supervision, to motivate staff.

- Feedback should be provided to staff in health facilities to acknowledge receipt of their reports. Epidemiological bulletins should also be shared back with staff at health facility level, in order to provide feedback on system performance and to provide key analysis.

___ 

### References 

!!! references
	1. Early detection, assessment and response to acute public health events: implementation of early warning and response with a focus on event-based surveillance. Geneva, World Health Organization, 2014.
	1. A Guide to establishing event-based surveillance. Manila, World Health Organization Regional Office for the Western Pacific, 2014.
	1. Public Health for mass gatherings: key considerations. Geneva, World Health Organization, 2015.
