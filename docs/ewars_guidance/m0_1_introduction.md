 <div class="guid-pdf"><a class="guid-pdf-link" href="/ewars_guidance/m0_1_introduction.pdf" target="__blank">Download <strong>PDF</strong></a></div>

# Introduction

## 1. What is EWAR?

- EWAR stands for "Early Warning, Alert and Response".
- It is one of the most immediate and important functions of surveillance.
___

## 2. What is the objective of EWAR?

!!! definition ""
	The objective of EWAR is to support the **early detection and rapid response to acute public health events of any origin**.

___

## 3. EWAR and Health Security

- The **2005 International Health Regulations** state that countries must strengthen their frontline capacity to detect, assess, notify, and report events involving disease or death above expected levels for the particular time and place in all areas of the country.

- The EWAR function of the surveillance system forms the backbone for achieving this core capacity. 

- EWAR emphasizes direct links between communities and all levels of the health system to achieve early detection and response to outbreaks and other public health emergencies.

___

## 4. What do we mean by an emergency?

- The WHO's Emergency Response Framework (ERF) states that WHO country offices must establish or strengthen EWAR within 7 to 14 days after the onset of an emergency being declared.

!!! more-details "An emergency may include one or more of the following:"

	- Complex humanitarian emergencies (situations of war or civil strife affecting large civilian populations with food shortages and population displacement)
	- Natural disasters (e.g. floods, tsunamis, earthquakes)
	- Food insecurity and famine
	- Large-scale disease outbreaks that overwhelm national capacity
	- Food contamination, chemical or radio-nuclear spills, and public health emergencies due to other hazards

___

## 5. Why is EWAR important in emergencies?

!!! more-details ""
	One of the most **urgent priorities in an emergency is to establish a functioning EWAR system** to rapidly detect and respond to outbreaks.

- In emergencies, the existing national surveillance systems may be underperforming, disrupted or non-existent.

- Emergencies also create risk factors for the transmission of communicable diseases which can result in high levels of excess morbidity and mortality. 

___

## 6. What are the main components of an EWAR system?

- EWAR can be most easily understood as a system composed of three core components.

![Basic](../assets/guidance/steps-basic.svg "Basic")

- In this guidance, each of these components is broken down and described in more detail.

| Component     | Description |
|---------------|-------------|
| **Early Warning** | Early warning refers to the organised processes to collect and report data from a range of different sources, in order to trigger potential alerts to public health events. <br><br>These sources of early warning data are categorised as **indicator-based surveillance** and **event-based surveillance**.  |
| **Alert** |  Alert refers to the systematic process of **verification and risk assessment of acute public health alerts** to initiate a rapid response where needed.<br><br>Regardless of the source of the information, all alerts should be managed according to a standardised workflow. |
| **Response** | Response refers to any **public health action** that is initiated based on the verification and risk assessment of alerts.<br><br>This guidance focuses on role of EWAR in strengthening surveillance duting the response to acute public health events. |

![Detailed](../assets/guidance/steps-detailed.svg "Detailed")

<!--_
___

## 7. What is an EWAR network?

- EWAR relies on a network of human resources responsible for the collection, reporting, analysis and sharing of information.

- It requires staff working from a range of backgrounds and disciplines, working at different levels through the reporting chain from community and health facility level, up to the national level.

- This network of people needs to be adequately trained and supported to perform their functions throughout the duration of the emergency.

__

### Coordination mechanisms and EWAR systems

Emergency responses are coordinated by different mechanisms, depending on the nature of the event, which relate directly to EWAR:

- In a complex humanitarian emergency or natural disaster, the Health Cluster may be activated under the leadership of WHO and the Ministry of Health (MoH) to coordinate the activities of partners;

- In a large-scale outbreak, the MoH and WHO may operationalize the response through an Incident Management System (IMS) and/or a pillar approach (to coordinate across cross-cutting technical issues);

- A combination of both approaches (e.g. large-scale cholera outbreak during a complex humanitarian emergency in Yemen) may necessitate both coordination mechanisms (e.g. Health Cluster and IMS).

Typically, the Health Cluster or IMS, led by the WHO country office with support from the MoH, NGO partners and/or the Centers for Disease Control and Prevention (CDC) leads the development of the EWAR system.

THe EWAR system may remain in place for years after the acute emergency for which is was intended. However, the EWAR system should be integrated into the national surveillance system rather than being used to replace it. Linkages to the surveillance system should be developed. An exit or transition strategy is important to plan from the start.


!!! note "References"
	1. [Clara, Alexey, Trang T. Do, Anh T. P. Dao, Phu D. Tran, Tan Q. Dang, Quang D. Tran, Nghia D. Ngu, et al. 2018. &quot;Event-Based Surveillance at Community and Healthcare Facilities, Vietnam, 2016-2017.&quot;](http://paperpile.com/b/JgBZMP/Vwir)[_Emerging Infectious Diseases_](http://paperpile.com/b/JgBZMP/Vwir)[24 (9): 1649–58.](http://paperpile.com/b/JgBZMP/Vwir)
	2. [Diggle, Emma, Wilhelmina Welsch, Richard Sullivan, Gerbrand Alkema, Abdihamid Warsame, Mais Wafai, Mohammed Jasem, Abdulkarim Ekzayez, Rachael Cummings, and Preeti Patel. 2017. &quot;The Role of Public Health Information in Assistance to Populations Living in Opposition and Contested Areas of Syria, 2012-2014.&quot;](http://paperpile.com/b/JgBZMP/4oU0)[_Conflict and Health_](http://paperpile.com/b/JgBZMP/4oU0)[11 (December): 33.](http://paperpile.com/b/JgBZMP/4oU0)

-->