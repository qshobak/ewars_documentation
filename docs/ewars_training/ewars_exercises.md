<style>

.card_row {
	width: 100%;
    height: 100px;
    border-radius: 4px;
    padding: 8px;
    margin: 4px;
    background-color: rgba(55,178,244,.08);

}

.card-icon-row {
	width: 60px;
    height: 60px;
    font-size: 18px;
    text-align: center;
    line-height: 60px;
    color: #fff;
    border-radius: 50%;
    background-color: #37b2f4;
}


.card-row__centre {
    /*display: flex;*/
    align-items: left  !important;
    justify-content: left  !important;
    width: 65%  !important;
}

.card-row__left {
	    float: left;
	   	margin: 2% 0 0 2%;
}

.card-row__right {
	    float: right;
}

.card-row-centre-head {
	    margin: -33px 0 0 20%;

}

.card-row-centre-head-desc {
	    margin: -15px 0 0 20%;
	    font-size: 12px !important;
    	font-weight: normal !important;
    	line-height: 14px;
}


.card-row-centre-head-title {
    /*display: flex;*/
    font-size: 18px  !important;
    color: #3f424f  !important;
    font-weight: 700  !important;
    text-align: left  !important;
    line-height: 18px !important;
}

.card-row__btn {
	width: 96px;
    height: 26px;
    line-height: 26px;
    border-radius: 2px;
    background-color: #fff;
    font-size: 12px;
    font-weight: 400;
    padding: 0 8px;
    margin: -2px auto 0;

}

.card-row__btn.top {
	margin: -33px 0 10px 0px;

}

.card-row__btn.na {
	width: 120px;
	background: #bebebe;
	color: white;
	text-align: center;
}


.card-desc {

	margin: 0  !important;
}

.material-icons {
    width: 75%;
    margin: 8px 0 0 8px;
}

.card-icon-row.md {
    line-height: 114px !important;
}

@media screen and (max-width: 600px) {
  .card-row-centre-head-desc {
    display: none;
  }

.card-row-centre-head-title {
      margin: 27% 0 0 17% !important;
  }
}

 </style>

# EWARS-in-a-box exercises  


<div class="home-page">
    <h1></h1>
    <div class="card_row card--t-d">
    	<div class="card-icon-row card-row__left">
    		1
    	</div>
    	<div class="card-row__centre">
	    	<div class="card-row-centre-head">
	    		<h2 class="card-row-centre-head-title"><!--a href="../presentations/p_ex_getting_started.html" target="_blank"></a-->1. Getting started</h2>
	    	</div>
	    	<div class="card-row-centre-head-desc">
	    		<p class="card-desc">You just arrived in Nambutu. You are working for an NGO and want to start reporting into EWARS-in-a-box.</p>
	    	</div>
	    </div>
        <div class="card-row__btn na card-row__right">Not yet available
            <!--a href="../presentations/p_ex_getting_started.html?print-pdf" target="_blank">Download <strong>PDF</strong></a-->
        </div>
    </div>
    <div class="card_row card--t-d">
        <div class="card-icon-row card-row__left">
        	2
        </div>
        <div class="card-row__centre">
	        <div class="card-row-centre-head">
	            <h2 class="card-row-centre-head-title"><!--a href="../presentations/p_m1_introduction.html" target="_blank"></a-->2. Reporting</h2>
	        </div>
	        <div class="card-row-centre-head-desc">
	            <p class="card-desc">Its time to submit your first weekly IBS report using EWARS-in-a-box. You are in a remote district with no internet connection.</p>
	        </div>
	    </div>
        <div class="card-row__btn na card-row__right">Not yet available
            <!--a href="../presentations/p_ex_getting_started.html?print-pdf" target="_blank">Download <strong>PDF</strong></a-->
        </div>
    </div>
    <div class="card_row card--t-d">
    	<div class="card-icon-row card-row__left">
    		3
    	</div>
    	<div class="card-row__centre">
	    	<div class="card-row-centre-head">
	    		<h2 class="card-row-centre-head-title"><a href="../presentations/p_ex_alert.html" target="_blank">3. Managing alerts</h2></a>
	    	</div>
	    	<div class="card-row-centre-head-desc">
	    		<p class="card-desc">As a district officer you are receiving alerts of a suspected cholera outbreak in a nearby IDP camp.</p>
	    	</div>    
	    </div>
    	<div class="card-row__right">
        <div class="card-row__btn top">
            <a href="../presentations/p_ex_alert.html?print-pdf" target="_blank">Download <strong>PDF</strong></a>
        </div>
         <div class="card-row__btn">
            <a href="../presentations/p_ex_alert.html" target="_blank">View <strong>online</strong></a>
        </div>
    	</div>
    </div>
    <div class="card_row card--t-d">
    	<div class="card-icon-row card-row__left">4</div>
    	<div class="card-row__centre">
    	<div class="card-row-centre-head">
    		<h2 class="card-row-centre-head-title"><a href="../presentations/p_ex_response.html" target="_blank">4. Outbreak response</h2></a>
    	</div>
    	<div class="card-row-centre-head-desc">
    		<p class="card-desc">A cholera outbreak has just been declared in Nambutu. You need to share your line-list of cases through EWARS-in-a-box.</p>
    	</div>    </div>
    	<div class="card-row__right">
        <div class="card-row__btn top">
            <a href="../presentations/p_ex_response.html?print-pdf" target="_blank">Download <strong>PDF</strong></a>
        </div>
         <div class="card-row__btn">
            <a href="../presentations/p_ex_response.html" target="_blank">View <strong>online</strong></a>
        </div>
    	</div>
    </div>
    <div class="card_row card--t-d">
        <div class="card-icon-row card-row__left">5</div>
        <div class="card-row__centre">
        <div class="card-row-centre-head">
            <h2 class="card-row-centre-head-title"><!--a href="../presentations/p_m1_introduction.html" target="_blank"></a-->5. Data analysis and use</h2>
        </div>
        <div class="card-row-centre-head-desc">
            <p class="card-desc">You need to rapidly analyse the cholera data for Nambutu in EWARS-in-a-box.</p>
        </div>
    </div>
        <div class="card-row__btn na card-row__right">Not yet available
            <!--a href="../presentations/p_m1_introduction.html?print-pdf" target="_blank">Download <strong>PDF</strong></a-->
        </div>
    </div>
    <div class="card_row card--t-d">
        <div class="card-icon-row card-row__left">6</div>
        <div class="card-row__centre">
        <div class="card-row-centre-head">
            <h2 class="card-row-centre-head-title"><!--a href="../presentations/p_m1_introduction.html" target="_blank"></a-->6. Administrator</h2>
        </div>
        <div class="card-row-centre-head-desc">
            <p class="card-desc">You are coordinating EWARS-in-a-box in Nambutu and are receiving requests to create new locations and reporting forms. </p>
        </div>
    </div>
        <div class="card-row__btn na card-row__right">Not yet available
            <!--a href="../presentations/p_m1_introduction.html?print-pdf" target="_blank">Download <strong>PDF</strong></a-->
        </div>
    </div>
 
</div>


