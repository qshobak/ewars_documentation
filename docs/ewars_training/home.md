<style>

.material-icons {
    width: 60%;

}

.card-icon.md {
    line-height: 114px !important;
}

.card-desc {

    margin: 0  !important;
    text-align: center;
}

</style>

#EWAR training materials
<div class="home-page">
    <h1></h1>
    <div class="card card__home card--t-b">
        <a href="/ewars_training/presentations.html">
            <div class="card-icon"><!--img class="material-icons" src="/assets/icons/training/baseline_slideshow_white_48dp.png"></img-->1</div>
            <h2 class="card__title">Presentations</h2>
        </a>
        <div class="card-desc">
            <p>Operational guidance in a format for training others</p>
        </div>
    </div>
    <div class="card card__home card--t-b">
        <a href="/ewars_training/case_studies.html">
            <div class="card-icon"><!--img class="material-icons" src="/assets/icons/training/baseline_location_on_white_48dp.png"></img-->2</div>
            <h2 class="card__title">Case Studies</h2>
        </a>
        <div class="card-desc">
            <p>Scenarios to test the use of operational guidance in an emergency</p>
        </div>
    </div>
    <div class="card card__home card--t-b">
        <a href="/ewars_training/ewars_exercises.html">
            <div class="card-icon"><!--img class="material-icons" src="/assets/icons/training/baseline_spellcheck_white_48dp.png"></img-->3</div>
            <h2 class="card__title">EWARS-in-a-box exercises</h2>
        </a>
        <div class="card-desc">
            <p>Scenarios to test skills using EWARS-in-a-box in an emergency</p>
        </div>
    </div>
</div>