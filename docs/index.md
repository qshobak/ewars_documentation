<style>

.material-icons {
    width: 60%;
    height: 60%;

}

.card-icon.md {
    line-height: 114px !important;
}
.home-page--bg {
    background-image: url("assets/images/group-1484.svg");
    background-repeat: no-repeat;
    background-position: top right;
}
</style>

### EWAR Operational Guidance
<div class="home-page--bg">
    <div class="home-page">
        <h1></h1>
        <!--div class="card card__home card--t-a">
            <img src="/assets/training/eiab.png"></img>
        </div-->
        <div class="card card__home card--t-a">
            <a href="/ewars_guidance/home.html">
                <div class="card-icon">A<!--img class="material-icons" src="/assets/icons/baseline_check_box_white_48dp.png"></img--></div>
                <h2 class="card__title">Operational guidance</h2>
                <p class="card__p">How to implement EWAR in emergencies</p>
            </a>
        </div>
        <!--div class="card card__home card--t-b">
            <a href="/ewars_training/home.html">
                <div class="card-icon">B<!--img class="material-icons" src="/assets/icons/baseline_school_white_48dp.png"></img></div>
                <h2 class="card__title">Training materials</h2>
                <p class="card__p">Presentations, exercises and case studies on EWAR in emergencies</p>
            </a>
        </div-->
    </div>
    <!--h3>EWARS-in-a-box</h3>
    <div class="home-page">
        <div class="card card__home card--t-c">
            <a href="/ewars_application/home.html">
                <div class="card-icon">C<!--img class="material-icons" src="/assets/icons/baseline_person_white_48dp.png"></img></div>
                <h2 class="card__title">User guidance</h2>
                <p class="card__p">Guidance on EWARS-in-a-box for field users</p>
            </a>
        </div>
        <div class="card card__home card--t-d">
            <a href="/ewars_documentation/home.html">
                <div class="card-icon">D<!--img class="material-icons" src="/assets/icons/baseline_devices_white_48dp.png"></img></div>
                <h2 class="card__title">Technical documentation</h2>
                <p class="card__p">Guidance on EWARS-in-a-box for developers</p>
            </a>
        </div>
    </div-->
</div>

<!--
___

!!! more-details "Feedback"
    Please send any comments to [support@ewars.ws](mailto:support@ewars.ws?subject=EWAR Resources Site)

<!--We are actively supporting translation into French and Arabic. If you would like to propose other languages, please [write and let us know](mailto:support@ewars.ws?subject=EWAR Translation)! >

!!! more-details "Contributions"

    All our materials are free for everyone to use.

    If you would like to actively contribute you can become a [contributor to our git repository](https://gitlab.com/ewars/ewars_documentation). 

-->







