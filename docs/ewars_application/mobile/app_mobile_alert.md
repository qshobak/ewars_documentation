# Alert management on mobile

## Overview

EWARS mobile lets you access your alert log on your phone whilst you are in the field, so you can rapidly be notified and follow-up on alerts as quickly as possible.

___

## Alert log

1\. The mobile application shows a list of all alerts current open or being monitored.

![Alert Panel](../../assets/screens/app/mobile/alert/a1_1.png#50 bordered "Alert Panel")

2\. Click on a button to open the relevant list of alerts in each category.

![Alert List](../../assets/screens/app/mobile/alert/a1_2.png#50 bordered "Alert List")

___

### Opening an alert

1\. To open an alert, click on the name of the alert in the list. This will open a panel to show you the details of the alert.

![Alert Details](../../assets/screens/app/mobile/alert/a2.png#50 bordered "Alert Details")

___

### Viewing the details of an alert

1\. The alert details includes information on:

| Tab | Description |
|:--|:--|
| Header| Name, location and date of alert|
| Alert workflow | Displays the stage that the alert is currently at in the workflow |
| Alert data | **Activity** shows the activity related to the alert<br> **Triggering report** opens the report which caused the alert to be generated<br>**Users** list all users who are invited to the alert |

___

## Alert Management

#### Stage 1. Verification

1\. Click on `Verification` to open the verification notes panel.

![Verification stage](../../assets/screens/app/mobile/alert/a2.png#50 bordered "Verification stage")

2\. Add notes on the verification of the alert. 

![Verification notes](../../assets/screens/app/mobile/alert/a3_1.png#50 bordered "Verification notes")

3\. When completed, click on Outcome to open the list of options.

![Verification actions](../../assets/screens/app/mobile/alert/a3_2.png#50 bordered "Verification actions")

4\. When complete, press `Submit verification` to enter your selection.

___

#### Stage 2. Risk Assessment

1\. After Verifiication, if an alert is progressed to the next stage of Risk Assessment it will appear active in the alert workflow.

![Risk Assessment Stage](../../assets/screens/app/mobile/alert/a4_0.png#50 bordered "Risk Assessment Stage")

2\. Click on `Risk Assessment` to open up the Risk Assessment panel.

![Risk Assessment notes](../../assets/screens/app/mobile/alert/a4_1.png#50 bordered "Risk Assessment notes")

3\. Add notes on the results of the risk assessment of the alert, based on hazard, exposure and context. 

4\. When complete, press `Submit verification` to enter your selection.


___

#### Stage 3. Risk Characterisation

1\. Following Risk Assessment, a level of risk needs to be assigned to an Alert. This is done at the Risk Characterisation stage.

![Risk Characterisation Stage](../../assets/screens/app/mobile/alert/a5_0.png#50 bordered "Risk Characterisation Stage")

2\. Click on `Risk Characterisation` to open up the Risk Characterisation panel.

![Risk Characterisation notes](../../assets/screens/app/mobile/alert/a5_1.png#50 bordered "Risk Characterisation notes")

3\. Select an option from the list to assign a likelihood and consequences of spread based on your Risk Assessment. The information will update to reflect the overall level of risk. 

4\. When complete, press `Submit characterization` to enter your selection.

___

#### Stage 4. Outcome

1\. The final step is to assign a final outcome to the alert. 

![Outcome Stage](../../assets/screens/app/mobile/alert/a6_0.png#50 bordered "Outcome Stage")

2\. Click on `Outcome` to open up the Outcome panel.

![Outcome notes](../../assets/screens/app/mobile/alert/a6_1.png#50 bordered "Outcome notes")

3\.Select an option from the list to assign a final outcome. 

![Outcome actions](../../assets/screens/app/mobile/alert/a6_2.png#50 bordered "Outcome actions")

4\. When complete, press `Submit outcome` to enter your selection.

Congratulations! You have just successfully managed an alert in EWARS! 


!!! tip
	All alerts that have finished the outcome stage on EWARS Mobile will be closed and not longer viewable on the phone.

___

### Re-opening an alert

1\. After a final outcome has been assigned to an alert, it will be closed. However, if new information becomes available - or if a repeat risk assessment is required - then it may be necessary to reopen an alert and to update information.

!!! warning "Warning"
	You currently cannot re-open an alert that has been closed on the mobile application. To re-open an alert, use the desktop application or write to your supervisor to request it to be opened.

