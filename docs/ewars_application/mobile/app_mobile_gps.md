# Collecting GPS coordinates on mobile

## Overview

EWARS Mobile is specifically designed to support data collection in remote locations, typically in health facilities, which are offline and only have intermittent access to data connection.

## How can EWARS Mobile help you collect GPS coordinates

- If you are assigning mobile phones with EWARS Mobile to health facilities - and you do not yet have a complete mapping of their latitude and longitude - then you can use the application to help you the collection these GPS coordinates.

### How does it work?

1\. Each health facility that is created in EWARS also has a field where you can record its GPS coordinates.

2\. When the heath facility is created, if this field is left blank then  any user who signs into the mobile application with permissions for that facility will be notified of this in a message.


### How can I collect the coordinates?

1\. In order to collect the GPS coordinates and make this message disappeat, you click on


2\. The application will then use the GPS built into the phone to obtain the GPS coordinates and store then in the boxes.

3\. Press `Save` to store the coordinates.

!!! warning
	You should only obtain the GPS coordinates when you are at the health faciliy, to ensure an accurate measrement

!!! tip
	After you collectteh coordinates, they will be submitted to the WARS erver the next time you synchronise the application (and the message prompting you to collect them will also disappear )


 from those health facilities so you can better analyse the geographical distribution of the data that you collec

To support the mapping of the data collected by the application, we also support the collection and storage of GPS coordinates from these locations.

___

## How EWARS Mobile uses GPS coordinates
t.




## Using EWARS mobile in a different languages

1\. EWARS mobile is currently supported in English, French and Arabic.

2\. To switch between different languages, click on the `Settings` button in the top left of the screen.

3\. In the settings menu, press `Language` to open a drop-down menu that lists the languages currently available.

4\. Select the language you wish to use and press `Save changes` to register the choice.

#### English

![EN](../../assets/screens/app/mobile/settings/s_en.png#50 bordered "EN")

#### French

![FR](../../assets/screens/app/mobile/settings/s_fr.png#50 bordered "FR")

#### Arabic

![AR](../../assets/screens/app/mobile/settings/s_ar.png#50 bordered "AR")

