# Creating and submitting reports on mobile

## Overview

EWARS mobile allows you to create and queue reports for submission on a phone whilst you are offline. The reports can then be synchronised and submitted whenever a mobile data or internet connection is available.

___

## Starting a report

### Open report

1\. EWARS Mobile will show you the forms you are able to report.

2\. Click on the form you wish to submit.

![Start report](../../assets/screens/app/mobile/report/r1.png#50 bordered "Start report")

___

### Create new report

1\. Click on `Create New` to start a new report.

![Create report](../../assets/screens/app/mobile/report/r2.png#50 bordered "Create report")

!!! warning 
	- If you need to have the correct assignment before you can report a form.
	- If you do not see your form in the list, write to your EWARS coordinator to request permission.

___

## Completing a report

### Select location

1\. Press `Location` to open the list of location(s) you have permissions to submit reports from.

![Press location](../../assets/screens/app/mobile/report/r3.png#50 bordered "Press location")

!!! warning 
	- You will only see locations that you have assignments for. If you wish to submit the report for other locations, you have to request a new assignment for them.
	- Account Administrators and Geographic Administrators will need to search for a reporting location from the full set of locaations under their responsibility. 

___


### Confirm location

1\. Select a location(s) from the list to confirm the location of the report.

![Select location](../../assets/screens/app/mobile/report/r4.png#50 bordered "Select location")

___

### Select date

1\. Click on Report date to enter the date of the report. 

![Select date](../../assets/screens/app/mobile/report/r5_1.png#50 bordered "Select date")

___

### Confirm date

1\. For weekly reports, select the epidemiological week from the list.

![Select week](../../assets/screens/app/mobile/report/r5_2.png#50 bordered "Select week")

2\. For reports with no fixed reporting date, select the correct date from the calendar.

![Select day](../../assets/screens/app/mobile/report/r5_3.png#50 bordered "Select day")


!!! tip 
	The most recent week is shown at the top of the list. Weeks or dates in the future do not appear. 

!!! warning 
	- Certain forms cannot be submitted for dates in the future. In this case, the report cannot be submitted until the date is in the past.

___

### Entering data

1\. In the data entry area of the report, the fields will indicate the type of information that needs to be entered (e.g. number, text)

2\. Enter the data in each field in the report. 

![Enter data](../../assets/screens/app/mobile/report/r6.png#50 bordered "Enter data")

!!! warning
	Some forms contain required cells must have values entered before they can be submitted. These will be highlighted in red if they are empty when you try to submit a form.  

!!! tip "Tip - Working with case-based data"
	- If you are working with case-based data, then you will be more likely require an indivdual record to be updated after it has been submitted (for example, an line list which needs a laboratory result or patient outcome to be updated)
	- In this case, we recommend using EWARS desktop to submit and manage your data, and not EWARS Mobile.
___

### Saving drafts 

To save a draft of your report to continue working on later:

1\. Press the `Submit` button at the top of the form.

![Submit report](../../assets/screens/app/mobile/report/r7.png#50 bordered "Submit report")

2\. Press the yellow `Save as draft` button to confirm you wish to send the report.

![Save draft](../../assets/screens/app/mobile/report/r8_1.png#50 bordered "Save as draft")

3\. You can then close the report and open it later from the Drafts folder. 

___

## Submitting reports

### Submitting a report when online

1\. When you are ready to send the report, press the `Submit` at the top of the form.

![Submit report](../../assets/screens/app/mobile/report/r7.png#50 bordered "Submit report")

2\. Press the green `Submit` button to confirm you wish to send the report.

![Save draft](../../assets/screens/app/mobile/report/r8_1.png#50 bordered "Save draft")

3\. __If you have a data or an internet connection__, you will directly see a `Syncing` screen. After the sync has finished, the report will be sent.

![Syncing](../../assets/screens/app/mobile/report/r8_2.png#50 bordered "Syncing")

!!! warning 
	- You **cannot** edit or delete reports after they have been submitted in the current version of EWARS Mobile.
	- Therefore, please ensure your reports are as accurate as possible before submitting. 
	- If you need to edit or delete them after submitting, then you will need to do so on EWARS Desktop or contact your supervisor.

___

### Submitting a report when offline

1\. When you are ready to send the report, __if you are working offline__ you can still press the `Submit` at the top of the form.

![Submit report](../../assets/screens/app/mobile/report/r7.png#50 bordered "Submit report")

2\. Press the green `Submit` button to confirm you wish to send the report.

![Submit](../../assets/screens/app/mobile/report/r8_1.png#50 bordered "Submit")

3\. __If you do not have an active connection__, you will see a message confirming that it will be added to a Queue:

![Queue message](../../assets/screens/app/mobile/report/r9_1.png#50 bordered "Queue message")

___

## Queued reports

### Reviewing reports in the queue

1\. If you submit reports whilst you are offline, they will be added to your queue. You can review the reports that are queued for submission at any time by opening the queue:

![Queue](../../assets/screens/app/mobile/report/r9_2.png#50 bordered "Queue")

### Submitting reports from the queue

1\. To submit reports that are queued, you need to first ensure yoy have an active connection and then press `Sync`. 

2\. This can be done by opening the queue and then pressing `Sync`

![Sync queue](../../assets/screens/app/mobile/report/r9_2.png#50 bordered "Sync queue")

3\.Or you can press the `Sync` button in the home screen

![Sync home](../../assets/screens/app/mobile/report/r1.png#50 bordered "Sync home")

___

### Making edits to reports in the queue

1\. Reports that are still in your queue and not yet submitted can be opened and edited if more changes are needed.

2\. To do this, click on the report in the queue and press `Move to drafts`.

![Queue move to drafts](../../assets/screens/app/mobile/report/r12_1.png#50 bordered "Queue move to drafts")

3\. You will recieve a message asking you to confirm you wish to `De-queue` the report and move it to drafts.

![Dequeue](../../assets/screens/app/mobile/report/r12_2.png#50 bordered "Dequeue")

4\. Press `Dequeue record` to move the report to the drafts folder, where you can continue to make edits.

![Queue to drafts](../../assets/screens/app/mobile/report/r12_3.png#50 bordered "Queue to drafts")

___

### Deleting a single report in the queue

1\. If you would like to delete report in your queue, click on the report in the queue and press `Delete record?`.

![Queue delete](../../assets/screens/app/mobile/report/r12_1.png#50 bordered "Queue delete")

2\. You will see a message asking you to confirm you wish to `Delete record?` the report.

![Queue delete confirm](../../assets/screens/app/mobile/report/r12_4.png#50 bordered "Queue delete confirm")

3\. Press `Delete record` to permanently delete the report from the phone.

___

### Deleting all reports in the queue

1\. If you would like to permanently delete all the reports in your queue, press `Clear queue`

![Clear queue](../../assets/screens/app/mobile/report/r9_2.png#50 bordered "Clear qeue")

!!! warning
	- This action will delete all the reports currently queued for submission and remove them from the phone.
	- If you like to just make changes to the report before re-submitting, you should first move them to drafts and then continue editing.

___

### Form validation rules

After you press the `Submit button`, EWARS will check to see if any validation rules have been broken. If so, then these will be flagged and need to be corrected.

Examples include:

- Location validation
![Location validation](../../assets/screens/app/mobile/report/r10_1.png#50 bordered "Location validation")

- Date validation
![Date validation](../../assets/screens/app/mobile/report/r10_2.png#50 bordered "Date validation")

- Field validation
![Field validation](../../assets/screens/app/mobile/report/r10_3.png#50 bordered "Field validation")

!!! warning
	- All data validation rules must be corrected before a report can be successfully submitted.

___


## Sent reports

### Sent folder

1\. After submission, you can view the submitted reports by looking in the Sent folder.

![Sent folder](../../assets/screens/app/mobile/report/r11_1.png#50 bordered "Sent folder")

2\. This will show you a list of all reports that were successfully sent from the phone.

![Sent reports](../../assets/screens/app/mobile/report/r11_2.png#50 bordered "Sent reports")

!!! tip 
	- If you are on a very unreilable connection, and would like to be fully sure that your report has been succesfully submitted, then check your `Sent folder` after submitting the report.
	- If you can see the report in your sent folder, then it has successfully been sent!

!!! warning
	- You can currently *not* reopen and edit reports after they have been `Sent`.	
	- Therefore please ensure your reports are as accurate as possible before submitting. 
	- If you need to edit or delete them after they have been sent, then you will need to use the Desktop version or contact a supervisor with access to it on the laptop.

___

### Clear sent folder

1\. Over time, the number of reports that appears in the `Sent` folder can grow very long.

2\. If you would like to clear the list, you can do so by pressing `Clear sent log`

![Clear sent log](../../assets/screens/app/mobile/report/r11_2.png#50 bordered "Clear sent log")




