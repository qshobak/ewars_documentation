# EWARS Mobile

<div class="home-page">
    <h1></h1>
    <div class="card card--t-b">
        <a href="/ewars_application/mobile/app_mobile_install.html">
            <div class="card-icon">1</div>
            <h2 class="card__title">Installation and upgrades</h2>
        </a>
        <!--div class="card__btn">
            <a href="/ewars_application/mobile/app_mobile_install.pdf">Download <strong>PDF</strong></a>
        </div-->
    </div>
    <div class="card card--t-b">
        <a href="/ewars_application/mobile/app_mobile_reporting.html">
            <div class="card-icon">2</div>
            <h2 class="card__title">Reporting</h2>
        </a>
        <!--div class="card__btn">
            <a href="/ewars_application/mobile/app_mobile_reporting.pdf">Download <strong>PDF</strong></a>
        </div-->
    </div>
    <div class="card card--t-b">
        <a href="/ewars_application/mobile/app_mobile_alert.html">
            <div class="card-icon">3</div>
            <h2 class="card__title">Managing Alerts</h2>
        </a>
        <!--div class="card__btn">
            <a href="/ewars_application/mobile/app_mobile_alert.pdf">Download <strong>PDF</strong></a>
        </div-->
    </div>
    <div class="card card--t-b">
        <a href="/ewars_application/mobile/app_mobile_admin.html">
            <div class="card-icon">4</div>
            <h2 class="card__title">Browsing reports</h2>
        </a>
        <!--div class="card__btn">
            <a href="/ewars_application/mobile/ewars_application/mobile/app_mobile_admin.pdf">Download <strong>PDF</strong></a>
        </div-->
    </div>
    <!--div class="card card--t-b">
        <a href="/ewars_application/mobile/app_mobile_gps.html">
            <div class="card-icon">4</div>
            <h2 class="card__title">Collecting GPS coordinates</h2>
        </a>
        <!--div class="card__btn">
            <a href="/ewars_application/mobile/ewars_application/mobile/app_mobile_gps.pdf">Download <strong>PDF</strong></a>
        </div-->
    </div-->
    <div class="card card--t-b">
        <a href="/ewars_application/mobile/app_mobile_language.html">
            <div class="card-icon">5</div>
            <h2 class="card__title">Language settings</h2>
        </a>
        <!--div class="card__btn">
            <a href="/ewars_application/mobile/ewars_application/mobile/app_mobile_language.pdf">Download <strong>PDF</strong></a>
        </div-->
    </div>
    <div class="card card--t-b">
        <a href="/ewars_application/mobile/app_mobile_settings.html">
            <div class="card-icon">6</div>
            <h2 class="card__title">Other settings</h2>
        </a>
        <!--div class="card__btn">
            <a href="/ewars_application/mobile/ewars_application/mobile/app_mobile_settings.pdf">Download <strong>PDF</strong></a>
        </div-->
    </div>
</div>
