# Language options on mobile

## Overview

EWARS mobile is currently supported in English, French and Arabic. 

!!! tip
    We are actively supporting translation into more languages. If you would like to support us, or propose other languages, please [write and let us know](mailto:support@ewars.ws?subject=EWAR Translation)! 

___

## Using EWARS mobile in a different languages

1\. To switch between different languages in EWARS mobile, click on the `Settings` button in the top left of the screen.

![Settings](../../assets/screens/app/mobile/settings/s_settings.png#50 bordered "Settings")

2\. In the settings menu, press `Language` to open a drop-down menu that lists the languages currently available.

![Language options](../../assets/screens/app/mobile/settings/s_language.png#50 bordered "Language options")

3\. Select the language you wish to use and press `Save changes` to register the choice.

#### English

![EN](../../assets/screens/app/mobile/settings/s_en.png#50 bordered "EN")

#### French

![FR](../../assets/screens/app/mobile/settings/s_fr.png#50 bordered "FR")

#### Arabic

![AR](../../assets/screens/app/mobile/settings/s_ar.png#50 bordered "AR")

