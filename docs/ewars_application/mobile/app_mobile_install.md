#Installation and upgrading

## Overview

EWARS Mobile is regularly updated with updates to features and performance. It is easy to install and receive upgrades automatically.

___


## How to install 

### From the Google Play Store

1\. To install from the Google Play Store, you must first have a registered Google Account

2\. Using your account go to Google Play and search for `EWARS Mobile`

3\. Click on Install and wait for the application to download.

___

### From your phone


1\. Using your mobile phone browser, go to [http://cdn.ewars.ws/apk/app-release.apk](http://cdn.ewars.ws/apk/app-release.apk)

2\. The EWARS Mobile APK file will automatically download and be saved to your and computer 

3\. Search for the APK file in the phone File Manager, and click to install

!!! tip
	- If there is no reliable internet connection in the location where mobile phones will be setup, then take a copy of the latest APK with you on a laptop. 
	- You can then transfer the file from the laptop to the phones, and install without any internet connection!

___


### From a laptop

1\. Go to [http://cdn.ewars.ws/apk/app-release.apk](http://cdn.ewars.ws/apk/app-release.apk)

2\. Save the APK on a USB or a laptop

3\. Connect the phone and transfer the file onto it

4\. Browse to find the APK on the phone and click to install

!!! tip
	- You may need to tether the Android Phone as a media device to be able to access the File Directory and copy the file onto it.

___


## How to upgrade

### Current verson

1\. The current version of EWARS Mobile is *version 33*.

2\. You can confirm the version number by checking the `About` menu.

![About](../../assets/screens/app/mobile/settings/s_about.png#50 bordered "About")

!!! tip
	- The most important number to check is the major version number (e.g. "v33").
	- The minor version numbers (e.g. "0.2") are less important and will be updated each time there is an automatic upgrade.

___ 

### Receiving updates

1\. When you have installed the latest **version 33**, you will automatically receive the latest updates.

2\. EWARS mobile will automatically check to see if a new update is available each time you open and each time you sync your application.

3\. If any update is available, then you will be notified and the app will restart after downloading the upgrade in the background.

!!! case-study "Important!"
	- If you are on the old version of EWARS Mobile version 32, then you will not be able to receive automatic updates.
	- **You must first upgrade to version 33**
	- You can easily identify if you are using the only version 32 by the purple theme

	#### --- OUTDATED version 32 - YOU MUST UPGRADE ---

	![old version](../../assets/screens/app/mobile/install/old_version.png#50 bordered "old version")

___

## Help and Support

If you need any help or support with EWARS Mobile you can write to:

1. Your EWARS Account Administrator
2. The EWARS helpdesk [support@ewars.ws](mailto:support@ewars.ws)

