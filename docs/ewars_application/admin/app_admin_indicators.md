# Indicators

Indicators provide a key link between raw data that is collected in forms, and how it is analysed and visualised.

The logic in a form is defined in `Administration > Forms` . Prior to creating this, you must first create the Indicators to link the raw data to. These are created in `Administration > Indicators`

You can also organise your indicators into folders and sub-folders, to help navigate and reference them more easily when linking them in analysis. 

```mermaid
graph LR
A[Forms] 
A --  Logic  --> B[Indicators]
B --  Widgets  --> C[Analysis]

```