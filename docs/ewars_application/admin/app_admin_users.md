# User Managment

## User types

There are 3 key types of user in EWARS:

|Type | Description | Example |
|:--|:--|:--| 
| `Reporting User` | Most basic type of user account. User requires assignments to be able to create and submit reports.|Frontline staff working for MoH/NGO in a health facility.|
| `Account Administrator` | Most powerful user account. Full ability to administer an EWARS account. | WHO or MoH EWARS Coordinator working at National level.|
| `Geographic administrator`| Ability to view all reports and alerts for a given location. Not able to view Administrative functionality. | National or sub-national staff following reporting or alerts in a given location. 

!!! tip
	The `Geographic administrator` role can be granted to staff working at national level, if they need to view all forms for all locations but do **not** require full administrative access to the account.

___

## User permissions

A summary of the key permissions available to each user type is shown below.

| Activity | Reporting User | Geo Admin | Account Admin | 
|--|--|--|--|
| Create, submit and edit reports |Only with assignments|Yes for locations|Yes|
| View alerts |Only with assignments|Yes for locations|Yes|
| View Administrative features | No | No | Yes|

___

### Onboarding new users

There are 2 key methods to onboard new users into EWARS:

- User sign up
- User invitation

___

### User sign up

New users can sign up for an account with EWARS using the registration form. This is available from the `Sign-up` link on the account home page.

After signing up for a new account, the Account Administrator will receive a Task Notification and can then approve the account request and assign the user account type.

>**Note:** Before a notification is sent to an Account Administrator for account approval, a new user must verify the email address used to sign up for an account. 
>
>This can be done by clicking a link in a verification email sent upon sign up. After this is done, the task will appear for the Account Administrator and the account can be approved.

___

### User invitation

New users can be proactively invited to the EWARS application by Account Administrators. This is available from `Administration > Users > Invitation`

The user account type can be defined at the time the invitation is sent. After it is sent, the user will be prompted to complete the registration form and can then directly access the account. No further approvals are needed.

___

## System accounts 

It is strongly recommended that individual user accounts are created for each reporting unit in a given account. For example, this would mean creating an account for each health facility in a given EWARS deployment.

Ideally, these should be created using User sign-up or User invitation as described above.

However, in many emergencies the health staff at health facility level either dont have email accounts and/or it isn't feasible to expect them to complete a sign up procedure and create accounts themselves.

For this reason, system accounts can be used to rapidly create user accounts needed for a deployment. These can be created from  `Administration > Users > System accounts`

By default, all system email addresses are created with the suffix  `@ewars.ws`
The rest of the email address can be customised and passwords assigned at the time the accounts are created. 

| Advantage | Disadvantage | 
|--|--|
| Can be created rapidly | Can lead to lengthy email addresses needing to be created to be unique for each health facility
| Users do not need to sign up with real email addresses | Users unable to receive email notifications (e.g. alerts)| 

!!! admin "Advanced - User import"
	- In large deployments, there often isn't time to create potentially 100s or 1000s of user accounts for each health facility that will be involved in reporting. 
	- To help with this, user accounts can be scripted into EWARS based on a csv template. For more help with this, please write to support@ewars.ws
