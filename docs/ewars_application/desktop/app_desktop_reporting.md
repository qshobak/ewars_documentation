# Creating and submitting reports

## Overview

EWARS Desktop allows you to create, edit and delete reports from a laptop. You can work fully offline - and even collaborate on a dataset using a local peer-to-peer network - and then sync your changes when you next have an internet connection.

___

## Open Report Manager

To open the Report Manager:

1\. Go to `Data Collection > Reports Manager`,or
2\. Click on the `Report Manager` shortcut icon in the menu bar.

___

## Report Manager

In the Report Manager:

1\. The left menu shows the list of reporting forms that you have permission to use.
2\. If you are an Account Administrator or Geographic Administrator, then you will automatically have permission to use all active Reporting Forms in an account.
3\. If you are a Reporting User, then you must first request Assignment for specific form(s) in specific location(s) before you are able to use them.

___

## Creating a report

- Click on the `Create New Report` button to open a blank form.

___

### Entering a report location

1\. Enter the location for the report by clicking on the location dropdown menu. This will list the locations you have permission to submit reports from.

![Location menu](../../assets/screens/app/desktop/report/r6.png#50 bordered "Location menu")

!!! tip
	- You will only see locations that you have assignments for. If you wish to submit the report for other locations, you have to request a new assignment for them.
	- If you open a report from Upcoming or Overdue list of reports in the Main Dashboard, the location will automatically be entered.
	- Account Administrators and Geographic Administrators will need to search for a reporting location from the full set of locaations under their responsibility. Valid locations that can be selected and reported from will appear in green. menu under their that can be selected

![Valid locations](../../assets/screens/app/desktop/report/r7.png#50 bordered "Valid locations")

___

### Entering a report date

1\. To enter the report date, click on the calendar icon in the Report Date field.
Select the applicable reporting interval from the Reporting Calendar    

![Report date](../../assets/screens/app/desktop/report/r8.png#50 bordered "Report date")

!!! tip
	- If you open a report from Upcoming or Overdue list of reports in the Surveilance Dashboard, the report date will automatically be entered.
	- Certain forms cannot be submitted for dates in the future. In this case, the report cannot be submitted until the date is in the past.

___

### Entering data

1\. In the data entry area of the report, the fields will indicate the type of information that needs to be entered (e.g. number, text)

![Data entry](../../assets/screens/app/desktop/report/r3.png# bordered "Data entry")

!!! tip
	- Some forms contain required cells must have values entered before they can be submitted. These will be highlighted in red if they are empty when you try to submit a form.   

___

### Saving drafts 

To save a draft of your report to continue working on later:

1\. Click on the `Save Draft` button. 
2\. You can then close the report and open it later from the Drafts folder. 

![Save draft](../../assets/screens/app/desktop/report/r3.png# bordered  "Save draft")

!!! warning
	- Before you save a draft, you must first enter the Location and Report date.

___

### Submitting a report 

When a report is ready to be submitted:

1\. Press the `Submit button`
2\. Click OK in the Submit Report box that appears to confirm you wish to send the report

![Submit](../../assets/screens/app/desktop/report/r3.png# bordered  "Submit")

___

### Form validation rules

1\.After you press the Submit button, the system will check to see if any validation rules have been broken. If so, then these will be flagged and need to be corrected.

2\. Examples include:

- a. Data field validation

![Field validation](../../assets/screens/app/desktop/report/r10.png#50 bordered "Field validation")

- b. Future date validation

![Date validation](../../assets/screens/app/desktop/report/r9.png#50 bordered "Date validation")

- c. Duplicate report validation

![Duplicate](../../assets/screens/app/desktop/report/r11.png#50 bordered "Duplicate")

!!! warning
	- All data validation rules must be corrected before a report can be successfully submitted.

___

### Congratulations!

- You have now successfully submitted your report!

___


## Browsing reports

1\. Clicking on the `Browse button` will open a list of all submitted reports that you have permissions to view. 

![Browse reports](../../assets/screens/app/desktop/report/r4.png# bordered  "Browse reports")

2\. You can open a Report by double clicking on it in the list.

___

### Editing reports 

To edit a report after it has been submitted:

1\. Open a report and and press `Amend Report`

2\. The data entry area will become editable, and you can make any required changes to the values.

![Amend report](../../assets/screens/app/desktop/report/r12.png# bordered  "Amend report")

3\. Add a reason for the amendment into the description box.

![Amend reason](../../assets/screens/app/desktop/report/r13.png#50 bordered "Amend reason")

4\. After completing your edits, press the <i class="fal fa-paper-plane"></i> Submit Amendment button   

Notes: 
- Amendments to report are not automatically accepted in EWARS and must be approved by an Administrator. 
- Once an amendment request has been made, further attempts to edit to the same report cannot be made until the pending request has been approved or rejected. 

___

### Deleting reports 

To delete a report:

1\. Open the report in the Report Manager. This will show you the User who submitted the Report and the Date it was submitted

2\. Press the `Delete Report` button

![Delete report](../../assets/screens/app/desktop/report/r12.png# bordered  "Delete report")

3\. Add a reason into the `Delete Report` box

![Delete reason](../../assets/screens/app/desktop/report/r14.png#50 bordered "Delete reason")

4\. Press the `Submit button` to delete the report 

!!! warning
	- Requests to delete reports are **not** automatically accepted in EWARS and must be approved by an Administrator. 
	- Once an deletion request has been made, further attempts to delete the same report cannot be made until the pending request has been approved or rejected. 

!!! references "Related guidance"
	- [Managing report deletion requests](#/task-management/4-report-deletion-requests) 
