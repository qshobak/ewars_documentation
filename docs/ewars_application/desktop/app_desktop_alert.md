# Alert Management on desktop

## Overview

EWARS-in-a-box lets you access your alert log on your laptop whilst you are offline, so you can rapidly be notified and follow-up on alerts as quickly as possible.

___

## Alert dashboard

1\. The alert dashboard provides an overview of all alerts that are active and inactive in EWARS.
To open the alert dashboard, click on the `Alert tab`.

2\. Alternatively you can click on the `short-cut in the top menu bar`.

!!! admin
	The settings to define the alert thresholds for each type of alert in EWARS is controlled in `Administration > Alarms`

___

## Alert log

1\. The default view of alerts is in the form of an Alert Log.
This shows a list of all alerts and key information about them.

2\. You can move between active and closed alerts, by selecting the relevant tabs.
To view alerts in a map view, click on the map tab.

!!! admin
	The settings to control which geographic level of alerts in the map view is controlled in `Administration > Settings > Alarms`

___

### Opening an alert

1\. To open an alert, double click the row or click the `view icon`.
This will open a panel to show you the details of the alert.

___

### Viewing the details of an alert

1\. The alert panel includes a number of tabs:

![Alert Panel](../../assets/screens/app/desktop/alert/a1.png# bordered  "Alert Panel")

| Tab | Description |
|:--|:--|
| Overview| Status of the alert according to the alert workflow|
| Records | Shows the record(s) that triggered the alert
| Activity| Shows comments and actions taken so far on the alert|
| Analysis| Displays a graph with an historical trend for the alert|
| Users| Lists the users current invited to the alert|

___

## Alert Management

#### Stage 1. Verification

1\. Click on `Start Verification` to open up the Verification notes panel.

![Verification notes](../../assets/screens/app/desktop/alert/a2.png# bordered  "Verification notes")

2\. Add notes on the verification of the alert. When completed, click on the relevant outcome:

1. Discard
2. Monitor
3. Start Risk Assessment

3\. Use the buttons above to Submit, Save as Draft or Cancel.

![Verification actions](../../assets/screens/app/desktop/alert/a3.png#50 bordered "Verification actions")

___

#### Stage 2. Risk Assessment

1\. After Verifiication, if an alert is progressed to the next stage of Risk Assessment it will appear active in the alert workflow.

![Risk Assessment Stage](../../assets/screens/app/desktop/alert/a4.png# bordered  "Risk Assessment Stage")

2\. Click on `Start Risk Assessment` to open up the Risk Assessment panel.

![Risk Assessment notes](../../assets/screens/app/desktop/alert/a5.png# bordered  "Risk Assessment notes")

3\. Add notes on the results of the risk assessment of the alert, based on hazard, exposure and context. 

4\. When completed, use the buttons above to Submit, Save as Draft or Cancel.

![Risk Assessment actions](../../assets/screens/app/desktop/alert/a3.png#50 bordered "Risk Assessment actions")

___

#### Stage 3. Risk Characterisation

1\. Following Risk Assessment, a level of risk needs to be assigned to an Alert. This is done at the Risk Characterisation stage.

![Risk Assessment Stage](../../assets/screens/app/desktop/alert/a6.png# bordered  "Risk Characterisation Stage")

2\. Click on `Start Risk Characterisation` to open up the Risk Characterisation panel and to see the Risk Matrix.

![Risk Assessment notes](../../assets/screens/app/desktop/alert/a7.png# bordered  "Risk Characterisation notes")

3\. Click on a cell within the Risk Matrix to assign a level of risk. The panel on the right will update to reflect the likelihood, consequences and actions that need to be taken based on each level of risk.

4\. When completed, use the buttons above to Submit, Save as Draft or Cancel.

![Risk Assessment actions](../../assets/screens/app/desktop/alert/a3.png#50 bordered "Risk Characterisation actions")

___

#### Stage 4. Outcome

1\. The final step in the alert management process is to assign a final outcome to the alert. Following Risk Characterisation, the alert will automatically be progressed to the stage of Outcome and it will appear active in the alert workflow.

![Outcome Stage](../../assets/screens/app/desktop/alert/a8.png# bordered  "Outcome Stage")

2\. Click on `Start Risk Outcome` to open up the Risk Characterisation panel and to see the Risk Matrix.

![Outcome notes](../../assets/screens/app/desktop/alert/a9.png# bordered  "Outcome notes")

3\. Click on a cell within the Risk Matrix to assign a level of risk. The panel on the right will update to reflect the likelihood, consequences and actions that need to be taken based on each level of risk. 

4\. When completed, use the buttons above to Submit, Save as Draft or Cancel.

![Outcome actions](../../assets/screens/app/desktop/alert/a3.png#50 bordered "Outcome actions")

5\. **Congratulations!** You have just successfully managed an alert in EWARS! All alerts that have completed the outcome stage will now be moved to the `Closed alerts` tab.

___

### Re-opening an alert

1\. After a final outcome has been assigned to an alert, it will be closed. However, if new information becomes available - or if a repeat risk assessment is required - then it may be necessary to reopen an alert and to update information.

2\. To re-open an alert, go to the `closed Alerts tab` and open to view the details of the alert.

![Reopen alert](../../assets/screens/app/desktop/alert/a10.png# bordered  "Reopen alert")

3\. Click on the `Re-open alert` button. You will then also need to enter a reason why the alert is being re-opened.

![Reopen reason](../../assets/screens/app/desktop/alert/a11.png# bordered  "Reopen reason")

4\. Enter the reason and click on `Re-open alert` in the open box to confirm and move the alert back to the Open alerts tab. It will be reset back to the Verification stage, and you can review and edit the information entered in all the previous stages.

