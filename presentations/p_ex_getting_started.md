# Training Scenario

## 1. Creating an account
___
###**Task 1.1** | Creating your user account

- Go to the website [http://cg.ewars.ws](http://cg.ewars.ws)

- Click on the link "Create an Account" and enter your details and choose a password 

- Select the appropriate country for your account.  

><i class="fa fa-cog"></i>  Wait for the MoH Administrator to approve your user account. 
- When approved you will receive an email asking you to verify your account. Click on the link in the email to activate the account.

## 2: Creating and submitting weekly EWARS reports online
___

###**Task 2.1** | Requesting and approving assignments


- Sign in with your new username and password and request the assignment for the Weekly IDSR Form for your location. 

><i class="fa fa-cog"></i> Wait for an Administrator to approve your assignment request.


###**Task 2.2** | Submitting a report
___


- After your assignment is approved, submit a **Weekly IDSR Report** for your health facility for the current epidemiological week. 

- Try to submit the form without completing all the fields. What happens? 

- After you submit, try to create a **new** report again for the same health facility in the same reporting week. What happens? 


## 3: Editing, Deleting, Tracking Reports online
___
###**Task 3.1** | Tracking missing reports
___

- Open the M&E Auditor and view **Completeness** for the **Weekly IDSR Reports** in your province. What is your completeness for W40?  
- If you have time, feel free to practice submitting other reportsfor any missing health facilities in your province.

###**Task 3.2** | Editing and deleting report You&#39;ve realised you made a mistake in Weekly IDSR Report that was submitted.
___

- Open the Report Manager and browse to find the report that you submitted. 

- Double click to edit the report, change the appropriate value, and submit an amendment. 

><i class="fa fa-cog"></i>
 Wait for the MoH Administrator to approve the amendment request in your tasks list.    

## 4: Setting up EWARS on the phone
___
**Tip 1** | Preparing the phone

- Before the phones can be distributed and used by partners, they need to be configured. Collect one phone each from the kit for use in your health facility.

!!! note

	- You can use any Android phone to install EWARS - even your own!
	- If a local SIM card is available, please enter it now. If not, then you will need to connect to the WiFi.

| **Tip 2** | Download and install the EWARS Mobile App

You can install the EWARS Mobile App in 2 ways:
1. by downloading from Google Play, or

1. by downloading as an APK file from this link and installing locally:
[http://cdn.ewars.ws/apk/app-release.apk](http://cdn.ewars.ws/apk/app-release.apk) The second option is useful if you need to install the app quickly on many phones, before they are distributed. Use this method to install the app on your phone. 


!!! checklist "Tip: Labelling and tracking the phones"

	- Normally, we label phones before they are distributed.  This way, its clear to users which phone is which. 
	- We also track who is using which phone and where. 

## 5: Data collection and reporting on the phone
___
**Task 5.1** | Signing in

- Turn your phone on, and sign in using your username and password. 

**Task 5.2** | Browsing reports
- Open the **Weekly IDSR Report** and browse to view the most recent reports that have been submitted. 
**Task 5.3** | Saving drafts and submitting reports
- For your health facility, create a new **Weekly IDSR Report**. Enter 2 measles cases, and save as drafts to submit later.   
- Open the report from your drafts folder and submit it. 

## 6: Management of alerts online and on the phone
___
**Task 6.1** | Triggering an alert

- After submitting your **Weekly IDSR Report** , check your alert log online or on the phone.  
**Task 6.2** | Verifying the alert on mobile

- Open the alert that was generated on the phone or online. View the steps involved in alert management. Complete the verification step and confirm that this is a real case. |
**Task 6.3** | Risk Assessment of alert
- At this stage you would complete the Risk Assessment, using the approaches you learned in the first training. Discuss with your group and complete the **Risk Assessment** , **Risk Characterisation** and **Risk Outcome** for this alert.     
- When you have completed these tasks online, switch and repeat them again using the mobile phone (and vice-versa).


## 7: Publishing epidemiological bulletins and analysing data
___
**Task 7.1** | Viewing and sharing the bulletin
- Open the **Weekly IDSR Bulletin** to view automated analysis for your country level data. Share the link with yourself, as an example of how it can be shared with other colleagues in your country.   
- Print the bulletin to pdf, as an example of how it can be used to present from during meetings  

**Task 7.2** | Exporting data
- Use the export feature to download the **Weekly IDSR Report** data for **the whole of 2018** to xls **.** |

**Task 7.3** | Custom analysis
- Use the notebooks to create your own customised analysis. Set them to be shared with other colleagues. |
