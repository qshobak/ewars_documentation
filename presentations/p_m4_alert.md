
<!-- .slide: class="center" -->
<!-- .slide: data-background-color="#3784f4" -->
<!-- .slide: data-background-image="/assets/images/world-map-bg.svg" -->
<!-- .slide: data-background-position="left 40px top 40px" -->
<!-- .slide: data-background-size="52%" -->

# Guidance
<!-- .element style="text-align: center; margin-top: -150px;"-->

## Alert Management
<!-- .element style="text-align: center; margin-top: -0px;"-->

---

### Topic

![EWAR steps](../assets/guidance/steps-detailed.svg "EWAR steps")

<!-- .element  class="fragment fade-in-then-out" data-fragment-index="1" style="text-align: center; margin-top: -30px !important; float:left; margin-left: 200px"-->

![EWAR Response](../assets/guidance/steps-alert.svg "EWAR Alert")

<!-- .element class="fragment" data-fragment-index="2" style="text-align: center; margin-top: -500px !important; float:left; margin-left: 335px"-->

---

### Steps

- Alert management
- 1\.Create alert log
- 2\.Define alert management workflow
- 3\.Define verification procedures
- 4\.Standardise risk assessment
- 5\.Standardise risk characterisation
- 6\.Standardise outcome
- 7\.Form rapid response teams

<!-- .element class="success" style="line-height: 40px"-->

---

<!-- .slide: class="center" -->
<!-- .slide: data-background-color="#3784f4" -->
<!-- .slide: data-background-image="/assets/images/world-map-bg.svg" -->
<!-- .slide: data-background-position="left 40px top 40px" -->
<!-- .slide: data-background-size="52%" -->

# Step 1
<!-- .element style="text-align: center; margin-top: -150px;"-->

## Create alert log
<!-- .element style="text-align: center; margin-top: -0px;"-->

--


### What are alerts?

- Definition
- **Alerts** are the first hints of a larger public health problem. They represent the first pieces of sparse data or information that hint at a potential risk to health.

<!-- .element: class=" note fragment" -->

- Alerts come from either IBS and EBS sources. 

<!-- .element: class="fragment" -->

- All alerts detected by an EWAR system, regardless of the source, should be systematically documented in a central **alert log** and managed in the same, predictable manner. 

<!-- .element: class="fragment" -->

- This helps to ensure that all alerts are recorded and acted upon consistently, and that the process can be monitored.

<!-- .element: class="fragment" -->

--

### Alert log

- The alert log should record:
- - the nature of the alerts (e.g. date, type, location) and 
- - the actions taken (investigation, verification, response).

<!-- .element: class="more-details" -->

- The logs should be assessed daily to review outstanding actions. 

- Alert logs should be kept at both national and subnational level. 

- Summary information on the numbers of alerts triggered, and the actions taken, should be sent weekly to the next level in the system and reported in the Epidemiological Bulletin.

<!-- .element: class="fragment" -->

--

## Discussion

- How are you recording your alerts currently?

- How do you ensure all relevant users can track alerts and their progress at the same time?

---

<!-- .slide: class="center" -->
<!-- .slide: data-background-color="#3784f4" -->
<!-- .slide: data-background-image="/assets/images/world-map-bg.svg" -->
<!-- .slide: data-background-position="left 40px top 40px" -->
<!-- .slide: data-background-size="52%" -->

# Step 2
<!-- .element style="text-align: center; margin-top: -150px;"-->

## Define alert management workflow 
<!-- .element style="text-align: center; margin-top: -0px;"-->

--


### Alert management


- All alerts must be managed according to a standard set of steps:

  1. Verification
  2. Risk Assessment
  3. Risk Characterisation
  4. Risk Outcome

<!-- .element: style="float: left; width: 50%; margin-top:-10px" class="" -->


<div style="position:relative; width:640px; height:480px; margin:0 auto;">
  
![EWAR 2](../assets/guidance/steps-detailed-2-2.svg "EWAR 2") 

</div>

<!-- .element: style="float: right; width: 40%; margin-top:0px" class=""  -->


--

### Alert management

- All alerts should be verified, but only a small proportion will go on to require risk assessment. 

<!-- .element: class="" -->

- Even fewer events will be considered high or very high risk, and require large-scale response.

<!-- .element: class="" -->
<br>
<div style="position:relative; width:640px; height:480px; margin:0 auto;">
  
![Alert Circles 1](../assets/guidance/alert_circles-1.svg "Alert Circles 1") <!-- .element: class="fragment" style="position:absolute;top:0;left:0;" -->
![Alert Circles 2](../assets/guidance/alert_circles-2.svg "Alert Circles 2") <!-- .element: class="fragment" style="position:absolute;top:0;left:0;" -->
![Alert Circles 3](../assets/guidance/alert_circles-3.svg "Alert Circles 3") <!-- .element: class="fragment" style="position:absolute;top:0;left:0;" -->

</div>

--

### Where should alerts be managed?


- All steps in the workflow should be conducted as close to the field-level as possible. 

- Decentralizing verification and risk assessment helps to ensure the rapid follow-up and processing of all alerts. 


- Note
- - Staff at higher levels in the system should be able to access information on the alerts and monitor performance, but should only become directly involved if additional expertise or support is required.

<!-- .element: class="reference fragment" style="margin-top: 15px"-->

--

### Alert performance

- The speed and efficiency in which alerts are managed and moved through each step form the basis for the measurement of performance indicators.

| Stage | Responsibility | Performance |
| --- | --- | --- |
| Verification | County or District level | Within 48 hours of an alert being triggered |
| Risk assessment  | County or District level and/or higher level* | Within 48 hours of an alert being verified |
| Risk characterisation | By the risk assessment team | At the time of the risk assessment |
| Outcome | By the risk assessment team | At the time of the risk assessment |

- Note
- - Higher levels (such as Province or State) should support risk assessment of high threat alerts. These should be clearer indicated in Alert SOPs. 

<!-- .element class="reference fragment" style="margin-top: 15px" -->

--

### Discussion

- Do you have SOPs currently to describe how alerts are managed and at which level?

- How do the SOPs differ depending on the type of alert?


---

<!-- .slide: class="center" -->
<!-- .slide: data-background-color="#3784f4" -->
<!-- .slide: data-background-image="/assets/images/world-map-bg.svg" -->
<!-- .slide: data-background-position="left 40px top 40px" -->
<!-- .slide: data-background-size="52%" -->

# Step 3
<!-- .element style="text-align: center; margin-top: -150px;"-->

## Define verification procedures
<!-- .element style="text-align: center; margin-top: -0px;"-->

--


### Verification

- Definition
- - **Verification** describes the process used to determine whether or not an initial alert is valid and requires escalation to the next level of risk assessment. 
- - It should remain as quick and straightforward a process as possible.

<!-- .element: class="note" -->

- It is important to develop a structured process and set of questions to verify alerts.

- Depending on the setting, verification can include different means of collecting information (e.g. by telephone or through field visit). 

<!-- .element: class="fragment" -->

- All alerts must be verified as quickly as possible and at least within 48 hours of detection. 

<!-- .element: class="fragment" -->

--


### Key information to support verification of an alert

- The information submitted with an alert will depend on the information collected in the IBS and EBS forms in a given emergency.

<!-- .element: class="fragment" -->

- As a guiding principle it should include a number of required fields:

<!-- .element: class="fragment" -->

|Category| Data |
|--|--|
|**Origin**|- Name of reporter<br>- Contact information of the reporter (e.g. mobile number)<br>- Original source of information (if second-hand) |
|**Location**|- Location of alert (e.g. village, district, province, state) |
|**Description**|- Age distribution of cases (< 5 years / > 5 years)<br>- Outcomes of cases (cases / deaths)<br><span style="color: grey" class="fragment">- Date of onset of symptoms\*<br>- Symptoms and signs of cases (to consider differential diagnoses)\*<br>- Actions taken\*</span>|

<!-- .element: style="float: left; width: 80%; margin-top: 20px" class="fragment" -->

\* From EBS forms

<!-- .element: style="float: left; width: 100%; color: grey; font-size: 15px; margin-top:-1px;    margin-left: 500px;" class="fragment"  -->


--


### Questions to help determine the validity of an alert

|Category| Types of questions |
|--|--|
|**Source**<!-- .element: class="fragment"  -->| - Has the event been reported by an official source (e.g. local health-care centre or clinic, public health authorities, animal health workers)<!-- .element: class="fragment"  -->|
|**Frequency** <!-- .element: class="fragment"  --> |- Has the event been reported by multiple independent sources (e.g. residents, news media, health-care workers, animal health status)? <!-- .element: class="fragment"  -->|
|**Epidemiology**<!-- .element: class="fragment"  -->|- Does the event description include details about time, place and people involved (e.g. six people are sick and two died three days after a ending a local celebration on in community X)?<!-- .element: class="fragment"  -->|
|**Clinical details**<!-- .element: class="fragment"  -->|- Is the clinical presentation of the cases described (e.g. a cluster of seven people admitted to hospital with atypical pneumonia, of whom two have died)?<!-- .element: class="fragment"  -->|
|**Consistency**<!-- .element: class="fragment"  -->|- Has a similar event been reported previously (e.g. with a similar presentation, affecting a similar population and geographical area, over the same me period)?<!-- .element: class="fragment"  -->|

<!-- .element: style="float: left; width: 80%;" -->

--

### Outcomes of verification


- At the end of the verification stage, there are a number of possible actions that can be taken.


- Possible actions at the verification stage:
- - 1. **Discard**
- - 2. **Monitor**
- - 3. **Risk asssess**


<!-- .element: class="fragment more-details" -->

--

### Alert Workflow

<div style="position:relative; width:2000px; height:480px; margin:0 auto;">
  
![Alert Step 1](../assets/guidance/steps-alert-1.svg "Alert Step 1") <!-- .element: class="fragment fade-in-then-out" style="position:absolute;top:0;left:0;" -->
![Alert Step 2](../assets/guidance/steps-alert-2.svg "Alert Step 2") <!-- .element: class="fragment" style="position:absolute;top:0;left:0;" -->

</div>


---

<!-- .slide: class="center" -->
<!-- .slide: data-background-color="#3784f4" -->
<!-- .slide: data-background-image="/assets/images/world-map-bg.svg" -->
<!-- .slide: data-background-position="left 40px top 40px" -->
<!-- .slide: data-background-size="52%" -->

# Step 4
<!-- .element style="text-align: center; margin-top: -150px;"-->

## Conduct a risk assessment
<!-- .element style="text-align: center; margin-top: -0px;"-->

--

### When is it done?


- Risk assessment should be carried out within 48 hours of an alert being verified.

<!-- .element: class="fragment" -->

- At this point is referred to as an **event**.

<!-- .element: class="fragment" -->

- Definition
- - Events are verified alerts that warrant risk assessment.

<!-- .element: class="note fragment" style="margin-top: 30px"-->


--

### What does it involve?

- The purpose of a risk assessment is to rapidly characterize the probability that a verified alert will have a serious public health impact, and to help determine the actions needed to reduce this risk.

<!-- .element: class="fragment" -->

- Risk assessment may change in complexity depending on the needs of any given event. 

<!-- .element: class="fragment" -->

- The level of risk for a given event may also change over time, meaning that risk assessment may need to be repeated as soon as new information becomes available (e.g. new cases, new areas affected, changes in outbreak indicators).

<!-- .element: class="fragment" -->


--

### How is it done?

The level of risk is based the following factors:
<!-- .element: style="float: left; width: 100%;" class="fragment" -->
  1. Hazard
  2. Exposure
  3. Context

<!-- .element: style="float: left; width: 30%;" class="fragment" -->

![Alert Venn](../assets/guidance/risk_venn.svg "Alert Venn")

<!-- .element: style="float: right; width: 60%; margin-top: -90px !important" class="fragment" -->


--

### What data is collected?

- The key information collected under each factor will assist in determining whether the event meets the criteria for response, and potential notification through IHR to WHO. <!-- .element: class="fragment" -->

| **Components** | **Key information** | 
| --- | --- | 
| **Hazard assessment** <br>Identification of the hazard (i.e. cholera), the characteristics of a public health hazard and health effects.   |- Laboratory confirmation<br>- Otherwise, listing of possible causes based on clinical and epidemiological features<br> |
| **Exposure assessment** <br>Evaluation of the exposure of individuals and populations to likely hazards.<br>|- # of people likely exposed<br>- # exposed likely susceptible<br>- Vaccination coverage|
| **Context assessment** <br>Evaluation of the context which may affect either the transmission potential or overall impact of the event | - Environment (e.g. climate, vegetation, land use)<br>- Health and nutritional status<br>- Cultural practices and beliefs<br>- Infrastructure (access, services)<br>- Social context (e.g. ongoing civil war, refugee camp) |


<!-- .element: class="fragment" style="margin-top: 20px" -->

--

### Who is responsible?

- Standard operating procedures should define which types of alerts will require which level of support for risk assessment. 

<!-- .element: class="fragment" -->

- Depending on the context, health facility staff may have the capacity to risk assess certain types of alerts at their level. For other alerts, additional technical resources or expertise may be required from higher levels in the system.

<!-- .element: class="fragment" -->

- A Rapid Response Teams with appropriate level of specialization should be assembled to carry out a risk assessment.

<!-- .element: class="fragment" -->

- Note
- - More detailed risk assessments are needed when events appear particularly dangerous for public health (e.g. a cluster of suspected diphtheria cases among new arrivals in a refugee camp; reports of suspected EVD close to the borders of two countries).

<!-- .element: class="fragment reference" style="margin-top: 15px"-->


--

### Role of laboratory surveillance 

- Laboratories play an essential role in supporting EWAR by:

<!-- .element: class="fragment" data-fragment-index="1" -->

- **Generating alerts**. <!-- .element: class="fragment" data-fragment-index="2" ---> Rapid diagnostic tests sometimes play a role in generating alerts (e.g. positive cholera rapid diagnostic test among one or more suspect cases)

<!-- .element: class="fragment" data-fragment-index="3" -->

- **Helping to characterise a hazard**. <!-- .element: class="fragment" data-fragment-index="4" --> Laboratory confirmation often plays a pivotal role in the characterisation of a hazard during a risk assessment or intial public health investigation. 

<!-- .element: class="fragment" data-fragment-index="5" -->

- Support for laboratory testing and confirmation should be achieved as rapidly as possible, ideally at peripheral levels of the health system, to support characterisation of the level of risk and identification of the control measures required. 

<!-- .element: class="fragment" data-fragment-index="6" -->

--

### Alert Workflow

<div style="position:relative; width:2000px; height:480px; margin:0 auto;">
  
![Alert Step 1](../assets/guidance/steps-alert-1.svg "Alert Step 1") <!-- .element: class="fragment fade-in-then-out" style="position:absolute;top:0;left:0;" -->
![Alert Step 2](../assets/guidance/steps-alert-2.svg "Alert Step 2") <!-- .element: class="fragment fade-in-then-out" style="position:absolute;top:0;left:0;" -->
![Alert Step 3](../assets/guidance/steps-alert-3.svg "Alert Step 3") <!-- .element: class="fragment" style="position:absolute;top:0;left:0;" -->

</div>

---

<!-- .slide: class="center" -->
<!-- .slide: data-background-color="#3784f4" -->
<!-- .slide: data-background-image="/assets/images/world-map-bg.svg" -->
<!-- .slide: data-background-position="left 40px top 40px" -->
<!-- .slide: data-background-size="52%" -->

# Step 5
<!-- .element style="text-align: center; margin-top: -150px;"-->

## Standardise risk characterisation
<!-- .element style="text-align: center; margin-top: -0px;"-->

--

### Risk Characterisation


- Definition
- - **Risk characterisation** is the assignment of a level of risk to the event according to its likelihood of occuring and the resulting public health consequences.

<!-- .element: class="note fragment" -->

- For some events, the information is limited and/or the overall level of risk is obvious and can be characterized automatically. 

<!-- .element: class="fragment" -->

- However, a useful tool to support decision-making is a **risk matrix**.

<!-- .element: class="fragment" -->

--

### Risk Matrix

A risk matrix helps to organise the information collected during a risk assessment, in order to to answer two key questions: 

<!-- .element: class="fragment" -->

  - 1\. What is the likelihood of further spread? <!-- .element: class="fragment" -->

  - 2\. What would be the consequences (type and magnitude) to public health if this were to occur? <!-- .element: class="fragment" -->

<!-- .element: style="float: left ; width: 40%;" -->

![Risk Matrix](../assets/guidance/risk_matrix.svg "Risk Matrix")

<!-- .element: style="float: right ; width: 50%;" class="fragment"  --> 

--

### Levels of risk

![Low Risk](../assets/guidance/risk-low.svg "Low Risk") <!-- .element: style="float: left; margin-top: 0px !important; margin-left: 0px !important" -->

Managed in accordance with standard intervention protocols, routine control programs and regulations (eg, surveillance using routine surveillance systems) <!-- .element: class="fragment" -->

<!-- .element: style="float: ; width: 60%; margin-left: 200px !important" -->

![Moderate Risk](../assets/guidance/risk-mod.svg "Moderate Risk") <!-- .element: style="float: left; margin-top: 0px !important; margin-left: 0px !important" -->

The roles and responsibility of the response must be specified. Specific surveillance or control measures required (eg enhanced surveillance, supplementary vaccination campaigns) <!-- .element: class="fragment" -->

<!-- .element: style="float: ; width: 60%; margin-left: 200px !important" -->

![High Risk](../assets/guidance/risk-high.svg "High Risk") <!-- .element: style="float: left; margin-top: 0px !important; margin-left: 0px !important" -->

Special attention from management is needed: it may be necessary to put in place command and control structures; a series of additional control measures will be needed, some of which may have significant consequences<!-- .element: class="fragment" -->

<!-- .element: style="float: ; width: 60%; margin-left: 200px !important" -->

![Very High Risk](../assets/guidance/risk-vhigh.svg "Very High Risk") <!-- .element: style="float: left; margin-top: 0px !important; margin-left: 0px !important" -->

An immediate response is required even if the event is reported outside normal business hours. Immediate attention from the required senior management (for example, the command and control structure should be put in place within a few hours); the implementation of control measures with serious consequences is very likely<!-- .element: class="fragment" -->

<!-- .element: style="float: ; width: 60%; margin-left: 200px !important" -->

--

### How long does it take?


- The process should not be overly complicated or lengthy. 

<!-- .element: class="fragment" -->

- It should be guided by expert opinion of the risk assessment team, with attempts to reach consensus based on the evidence that is available.

<!-- .element: class="fragment" -->
- During discussions, team members should consider all types of consequences of the event in addition to the expected magnitude (e.g. morbidity and mortality).

<!-- .element: class="fragment" -->

- The risk matrix also helps to assess and document changes in risk as new information becomes available, and before and after control measures are implemented.

<!-- .element: class="fragment" -->

--

### Alert Workflow

<div style="position:relative; width:2000px; height:480px; margin:0 auto;">
  
![Alert Step 1](../assets/guidance/steps-alert-1.svg "Alert Step 1") <!-- .element: class="fragment fade-in-then-out" style="position:absolute;top:0;left:0;" -->
![Alert Step 2](../assets/guidance/steps-alert-2.svg "Alert Step 2") <!-- .element: class="fragment fade-in-then-out" style="position:absolute;top:0;left:0;" -->
![Alert Step 3](../assets/guidance/steps-alert-3.svg "Alert Step 3") <!-- .element: class="fragment fade-in-then-out" style="position:absolute;top:0;left:0;" -->
![Alert Step 4](../assets/guidance/steps-alert-4.svg "Alert Step 4") <!-- .element: class="fragment" style="position:absolute;top:0;left:0;" -->

</div>

---

<!-- .slide: class="center" -->
<!-- .slide: data-background-color="#3784f4" -->
<!-- .slide: data-background-image="/assets/images/world-map-bg.svg" -->
<!-- .slide: data-background-position="left 40px top 40px" -->
<!-- .slide: data-background-size="52%" -->

# Step 6
<!-- .element style="text-align: center; margin-top: -150px;"-->

## Standardise risk outcome
<!-- .element style="text-align: center; margin-top: -0px;"-->

--

### Risk Outcome


- The final step is a decision to assign a final risk outcome to the event, based on the results of the risk assessment and risk characterisation.

- Possible actions at the outcome stage:
- - 1. **Discard**
- - 2. **Monitor**
- - 3. **Respond**

<!-- .element: class="fragment more-details" -->


--

### Alert Workflow

<div style="position:relative; width:2000px; height:480px; margin:0 auto;">
  
![Alert Step 1](../assets/guidance/steps-alert-1.svg "Alert Step 1") <!-- .element: class="fragment fade-in-then-out" style="position:absolute;top:0;left:0;" -->
![Alert Step 2](../assets/guidance/steps-alert-2.svg "Alert Step 2") <!-- .element: class="fragment fade-in-then-out" style="position:absolute;top:0;left:0;" -->
![Alert Step 3](../assets/guidance/steps-alert-3.svg "Alert Step 3") <!-- .element: class="fragment fade-in-then-out" style="position:absolute;top:0;left:0;" -->
![Alert Step 4](../assets/guidance/steps-alert-4.svg "Alert Step 4") <!-- .element: class="fragment fade-in-then-out" style="position:absolute;top:0;left:0;" -->
![Alert Step 5](../assets/guidance/steps-alert-5.svg "Alert Step 5") <!-- .element: class="fragment fade-in-then-out" style="position:absolute;top:0;left:0;" -->
![Alert Step 6](../assets/guidance/steps-alert-6.svg "Alert Step 6") <!-- .element: class="fragment" style="position:absolute;top:0;left:0;" -->

</div>

---

<!-- .slide: class="center" -->
<!-- .slide: data-background-color="#3784f4" -->
<!-- .slide: data-background-image="/assets/images/world-map-bg.svg" -->
<!-- .slide: data-background-position="left 40px top 40px" -->
<!-- .slide: data-background-size="52%" -->

# Step 7
<!-- .element style="text-align: center; margin-top: -150px;"-->

## Form rapid response teams
<!-- .element style="text-align: center; margin-top: -0px;"-->

--

### Rapid response teams

- To ensure a predictable early response, a rapid response team (RRT) should be formed at each level of the system to support the risk assessment of alerts. <!-- .element: class="fragment" -->

- The role of the rapid response teams are to conduct risk assessments of verified alerts, and support additional public health investigations.<!-- .element: class="fragment" -->

--

### Team composition

- The composition of a team will depend on the nature of the threat being risk assessed. 

<!-- .element: class="fragment" -->
- This can commonly include the following:

<!-- .element: class="fragment" -->

|Function|Role|
|--|--|
| Epidemiology<!-- .element: class="fragment" -->|Provide understanding on infectious disease threats, sources and modes of transmission<!-- .element: class="fragment" -->|
| Laboratories and health services<!-- .element: class="fragment" -->|Support specimen collection and transport<!-- .element: class="fragment" -->|
| Clinicians <!-- .element: class="fragment" -->| Support case management, IPC, immunization<!-- .element: class="fragment" -->|
| Veterinarians<!-- .element: class="fragment" -->|For zoonotic threats where indicated<!-- .element: class="fragment" -->|
| Information and communication<!-- .element: class="fragment" -->|To support communication of results<!-- .element: class="fragment" -->|

<!-- .element: class="fragment" style="margin-top: 40px"-->

--

### Standard operating procedures

- It is important to have SOPs to guide how the RRTs will work.

- They should clearly define:
- - Who is a member of the RRT at each level of the system 
- - Which alerts will be responded to at which level 
- - The mechanisms for how an RRT can request support from higher levels 

<!-- .element: class="fragment more-details" -->


--

### Discussion

- Do you have SOPs currently to describe how RRTs are composed and how they will operate at each level?


---

### Recap

- Alert management
- 1\.Create alert log
- 2\.Define alert management workflow
- 3\.Define verification procedures
- 4\.Standardise risk assessment
- 5\.Standardise risk characterisation
- 6\.Standardise outcome
- 7\.Form rapid response teams

<!-- .element class="success" style="line-height: 40px"-->



