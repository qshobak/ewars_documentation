
<!-- .slide: class="center" -->
<!-- .slide: data-background-color="#3784f4" -->
<!-- .slide: data-background-image="/assets/images/world-map-bg.svg" -->
<!-- .slide: data-background-position="left 40px top 40px" -->
<!-- .slide: data-background-size="52%" -->

# Introduction
<!-- .element style="text-align: center; margin-top: -150px;"-->

---


### Note and Success Boxes

- Note // if no title still include the -
- - something
  - asdf
- new line

<!-- .element class="note" -->

- Success // if no title still include the -
- - something
  - asdf
- new line

<!-- .element class="success" -->

---

### Details, Case Study and Reference Boxes

- Details
- - something
  - asdf
- new line

<!-- .element class="more-details" -->

- Case Study
- - something
  - asdf
- new line

<!-- .element class="case-study" -->

- Reference
- - something
  - asdf
- new line

<!-- .element class="reference" -->

---

<!-- .slide: class="center dark" -->
<!-- .slide: data-background-color="#3f424f" -->
<!-- .slide: data-background-image="/assets/images/world-map-bg.svg" -->
<!-- .slide: data-background-position="left 40px top 40px" -->
<!-- .slide: data-background-size="52%" -->

# Dark Theme
<!-- .element style="text-align: center; margin-top: -150px;"-->

---

<!-- .slide: class="dark" -->
### What is EWAR?

EWAR stands for: **E**arly **W**arning, **A**lert and **R**esponse **S**ystem

**The objective of EWAR is to support the early detection and rapid response to acute public health events of any origin.**

This is one of the most immediate and important functions of a surveillance system.

Note: To protect the health and lives of its population, a health system must be able to _rapidly detect and respond_ to outbreaks and other public health emergencies. This role is performed by the early warning, alert and response (EWAR) function of a surveillance system.


---

<!-- .slide: class="dark" -->
### EWAR and emergencies

An EWAR system is particularly important in an emergency, when existing systems may be underperforming, disrupted or non-existent.

Emergencies also create risk factors for the transmission of communicable diseases which can result in high levels of excess morbidity and mortality. 

One of the first priorities in an emergency is, therefore, to establish an EWAR system to rapidly detect and respond to outbreaks.


--


## Types of emergencies

|Type|Example|
|--|--|
|Complex humanitarian emergencies|Situations of war or civil strife affecting large civilian populations with food shortages and population displacement|
|Natural disasters|e.g. floods, tsunamis, earthquakes|
|Food insecurity and famine||
|Large-scale disease outbreaks| that overwhelm national capacity|
|Others|e.g.Food contamination, chemical or radio-nuclear spills, and public health emergencies due to other hazards|


---


### EWAR basic flow

| Component | Description |
|--|--|
|<i class="fa fa-line-chart"></i> Early Warning | Early warning refers to the organised processes to collect and report data from a range of different sources, in order to trigger potential alerts to public health events.|
|<i class="fa fa-bell"></i> Alert | Alert refers to the systematic process of triggering, verifying, and managing acute public health alerts, to initiate a rapid response.|
|<i class="fa fa-map-marker"></i> Response| Response refers to any public health action that is initiated based on the detection, verification, and risk assessment of alerts. |

<!-- .element: style="float: left; width: 50%;" -->


![EWARS flow](/assets/slides/flow1.png "EWARS Basic Flow")

<!-- .element: style="float: right ; width: 50%;" class="img-50" --> 

---


### EWAR detailed flow
___
<br>


| Component | Description |
|--|--|
|<i class="fa fa-chart-line"></i> Early Warning | The sources of early warning data are categorised as **indicator-based surveillance** and **event-based surveillance**</p> |
|<i class="fa fa-bell"></i>Alert | Regardless of the source of the information, all alerts should be managed according to a standardised workflow. The key steps in the alert management workflow are **(1) verification**, **(2) risk assessment**, **(3) risk characterisation**, and **(4) outcome**.</p> |
|<i class="fa fa-map-marker"></i> Response| As part of a public health response, this guidance focuses on the following activities to enhance surveillance through EWAR: **(1) case-finding**, **(2) line-listing**, **(3) contact tracing**, and **(4) public health investigation**</p> |

<!-- .element: style="float: left; width: 50%;" -->



![EWARS flow](/assets/slides/flow2.png "EWARS Detailed Flow")

<!-- .element: style="float: right ; width: 50%;" class="img-50" --> 

---


### EWAR training modules

<br>
Modules are available in the following subjects:

1. Indicator-based surveillance
2. Event-based surveillance
3. Alert
4. Outbreak Response
5. Checklist for implementing EWAR 
6. EWARS-in-a-box


---


### Field data collection

<br>

- EWARS-in-a-box is a field-based tool that supports EWAR in emergencies.
- It includes an online, desktop and mobile application that can be rapidly configured and deployed within 48 hours of an emergency being declared.
- It is designed with frontline users in mind, and built to work in difficult and remote operating environments.

<!-- .element: style="float: left ; width: 40%;" --> 


![EWARS-in-a-box](/assets/slides/eiab.png "EWARS-in-a-box")

<!-- .element: style="float: right ; width: 50%;" class="img-50"--> 

---


### EWARS-in-a-box

<br>
It includes a kit of ruggedised, field ready equipment needed to establsh and manage EWAR activities in the field.

- Does not require:
  - 24 hour electricity
  - Internet connection

- Requires:
  - Intermittent mobile phone coverage
  - Intermittent internet


---

