
<!-- .slide: class="center" -->
<!-- .slide: data-background-color="#3784f4" -->
<!-- .slide: data-background-image="/assets/images/world-map-bg.svg" -->
<!-- .slide: data-background-position="left 40px top 40px" -->
<!-- .slide: data-background-size="52%" -->

# Guidance
<!-- .element style="text-align: center; margin-top: -150px;"-->

## Indicator-based surveillance 
<!-- .element style="text-align: center; margin-top: -0px;"-->

---

### Topic

![EWAR steps](../assets/guidance/steps-detailed.svg "EWAR steps")

<!-- .element  class="fragment fade-in-then-out" data-fragment-index="1" style="text-align: center; margin-top: -30px !important; float:left; margin-left: 200px"-->

![EWAR Response](../assets/guidance/steps-alert.svg "EWAR Alert")

<!-- .element class="fragment" data-fragment-index="2" style="text-align: center; margin-top: -500px !important; float:left; margin-left: 335px"-->

---

### Steps

- Indicator-based surveillance
- 1\. Agree on strategy
- 2\. Select priority diseases and other hazards
- 3\. Define case definitions
- 4\. Define alert thresholds
- 5\. Strengthen data collection and reporting

<!-- .element class="success" style="line-height: 40px"-->

---

<!-- .slide: class="center" -->
<!-- .slide: data-background-color="#3784f4" -->
<!-- .slide: data-background-image="/assets/images/world-map-bg.svg" -->
<!-- .slide: data-background-position="left 40px top 40px" -->
<!-- .slide: data-background-size="52%" -->

# The Basics
<!-- .element style="text-align: center; margin-top: -150px;"-->

--

### What is IBS?

- Definition
- **Indicator-based surveillance (IBS)** is the routine collection, monitoring, analysis, and interpretation of data from health facilities that is based on standardized case definitions.

<!-- .element: class=" note fragment" -->

- Typically infectious diseases (e.g. measles) are included in IBS. In some systems, non-infectious hazards (e.g. bird die-offs, chemical incidents), are reported.

<!-- .element: class="fragment" -->

- IBS data should be collected and analyzed each week.

<!-- .element: class="fragment" -->

- IBS is the most classically-used and visible approach for epidemiological surveillance. It generates the most data within an EWAR system. 

<!-- .element: class="fragment" -->

- IBS data is reported from a pre-determined, official network of health facilties.

<!-- .element: class="fragment" -->

--

### IBS in emergencies

- Should be implemented as soon as possible

<!-- .element: class="fragment" -->

- Should focus only on the key health problems during emergency phase

<!-- .element: class="fragment" -->

- Should be limited to public health threats that can and will be acted upon

<!-- .element: class="fragment" -->

- Should remain simple and flexible to respond to new health problems 

<!-- .element: class="fragment" -->

- Data analysis should occur as close to at field level as possible to trigger prompt public health action

<!-- .element: class="fragment" -->


--

### Key characteristics of IBS

| Characteristic | Description | |
| --- | --- | -- | 
| **Key strength**| To trigger alerts for _known_ diseases and conditions | |
| Strategy | Exhaustive or Sentinel | |
| **Data sources** | _Traditional_<br>- Healthcare facilities (public and private)<br>- Laboratories (public and private)|_Non-traditional_<br>- Mortality registries<br>- Animal health surveillance<br>- Medications sales data |
| **Characteristics** |- Aggregated data<br>- Well-structured and organized format for data collection and reporting<br>- Limited to 8 to 12 priority diseases<br>| - Predominantely used for infectious hazards<br>- Standardised case definitions<br>- Standardised alert thresholds |
| **Process** |- Systematic and regular data collection<br>- Weekly frequency<br>- Passive reporting |- Always the same reporting sites<br>- Emphasis on weekly monitoring of alert thresholds and triggering of alerts |

<!-- .element: style="width: 100%; line-height: 14px" -->


---

<!-- .slide: class="center" -->
<!-- .slide: data-background-color="#3784f4" -->
<!-- .slide: data-background-image="/assets/images/world-map-bg.svg" -->
<!-- .slide: data-background-position="left 40px top 40px" -->
<!-- .slide: data-background-size="52%" -->

# Step 1
<!-- .element style="text-align: center; margin-top: -150px;"-->

## Agree on strategy
<!-- .element style="text-align: center; margin-top: -0px;"-->

--


### Strategy for IBS

- There are two key strategies to consider when implementing IBS:
- - Sentinel surveillance
- - Exhaustive surveillance

<!-- .element: class="reference" -->


| Exhaustive surveillance | Sentinel surveillance|
| --| -- |
| - All health facilities in the region participate | - Only a few selected health facilities (aim for good quality, geographic spread) | 
| - More expensive |- Less expensive |   
| - Higher requirements for supervision | - Sufficient to show trends |
| - Increased potential to detect outbreaks on time | - May detect outbreaks late |

--

### Sentinel surveillance

<div style="position:relative; width:640px; height:480px; margin:0 auto;">

![Sentinel 1](../assets/guidance/surv-strat-sentinel-1.svg "Sentinel 1")

 <!-- .element: class="fragment" style="position:absolute;top:0;left:0;" -->

![Sentinel 2](../assets/guidance/surv-strat-sentinel-2.svg "Sentinel 2")

 <!-- .element: class="fragment" style="position:absolute;top:0;left:0;" -->

</div>

- Note
- - An sentinel surveillance strategy is more less expensive, but may detect outbreaks late


<!-- .element: class="reference" -->

--

#### Exhaustive surveillance

<div style="position:relative; width:640px; height:480px; margin:0 auto;">

![Exhaustive 1](../assets/guidance/surv-strat-exhaustive-1.svg "Exhaustive 1")

 <!-- .element: class="fragment" style="position:absolute;top:0;left:0;" -->

![Exhaustive 2](../assets/guidance/surv-strat-exhaustive-2.svg "Exhaustive 2")

 <!-- .element: class="fragment" style="position:absolute;top:0;left:0;" -->

</div>

- Note
- - An exhaustive surveillance strategy is more resource intensive, but has increased potential to detect outbreaks earlier

<!-- .element: class="reference" -->

--

### Choice of strategy

- The strategy should be decided rapidly at the onset of IBS implementation, based on the availability of resources and partners.

- The IBS network should always aim to collect accurate and reliable data


- Minimal criteria that health facilities should be assessed on include the availability of: 
- - Well-trained staff (e.g. clinicians, data manager and someone capable of data interpretation) and 
- - Adequate resources to ensure complete, reliable and regular reporting. 

<!-- .element: class="reference" -->

---

<!-- .slide: class="center" -->
<!-- .slide: data-background-color="#3784f4" -->
<!-- .slide: data-background-image="/assets/images/world-map-bg.svg" -->
<!-- .slide: data-background-position="left 40px top 40px" -->
<!-- .slide: data-background-size="52%" -->

# Step 2
<!-- .element style="text-align: center; margin-top: -150px;"-->

## Select priority diseases and other hazards
<!-- .element style="text-align: center; margin-top: -0px;"-->

--

### Select priority diseases and other hazards

- A maximum of **8 to 12 diseases and health conditions** should be monitored through IBS to avoid overwhelming the system. 

<!-- .element: class="fragment" -->

- The list should be revised regularly to reflect the epidemiological context and any new or re-emerging hazards.

<!-- .element: class="fragment" -->

--

### Categories to consider

From the outset, consider which diseases have:

- An **epidemic profile** (e.g. _epidemic_ cholera: no reported cases, then large, unpredictable outbreaks). 

- An **endemic profile** (e.g. malaria, meningococcal disease, _endemic_ cholera: ongoing transmission with seasonal epidemics). 

- Could **result from an emergency** and its mass movements of people from an endemic area to a non-endemic area (e.g. hepatitis E, malaria).  

--

### Criteria to guide selection of priority diseases and conditions

- Criteria
- - Does the **disease or health condition** have the significant potential for a high impact on morbidity, disability, and/or mortality?
- - Does the **disease** have significant potential for sudden epidemics (e.g. cholera) or ongoing transmission with seasonal epidemics (e.g. meningitis, measles)?
- - Have there been recent public health emergencies involving non-infectious hazards that warrant the inclusion of a **non-infectious hazard** (e.g. severe lead poisoning in Northern Nigeria)?
- - Is the disease a specific target of a national, regional or international **control programme**?
- - Will the information to be collected enable significant, rapid, and cost-effective **public health action**?

<!-- .element: class="more-details fragment" -->

--

### Typical list of diseases and hazards

- Typical list of applicable diseases and hazards.
- 1\. Acute flaccid paralysis (suspected poliomyelitis)
- 2\. Acute hemorrhagic fever syndrome (dengue, EVD, lassa fever, yellow fever)
- 3\. Acute jaundice syndrome (suspected hepatitis A and E)
- 4\. Acute respiratory infection (suspected pneumonia)
- 5\. Acute watery diarrhea (suspected cholera)
- 6\. Bloody diarrhea (suspected dysentery)
- 7\. Malaria (suspected or confirmed)
- 8\. Suspected measles
- 9\. Suspected meningitis

<!-- .element: class="case-study fragment" style="line-height: 40px"-->

- Note
- - Non-communicable diseases, mortality and malnutrition are special cases and ** are not** typically included in IBS. 

<!-- .element: class="reference fragment" -->

---

<!-- .slide: class="center" -->
<!-- .slide: data-background-color="#3784f4" -->
<!-- .slide: data-background-image="/assets/images/world-map-bg.svg" -->
<!-- .slide: data-background-position="left 40px top 40px" -->
<!-- .slide: data-background-size="52%" -->

# Step 3
<!-- .element style="text-align: center; margin-top: -150px;"-->

## Define case definitions
<!-- .element style="text-align: center; margin-top: -0px;"-->

--

### Case definitions

- Definition
- - A **case definition** is a set of diagnostic criteria that must be met for an individual to be regarded as a case of a particular disease for surveillance and outbreak investigation purposes. 

- Every priority disease or condition in EWAR must have an associated case definition


- Case definitions must be clear, appropriate and consistently applied. 


--

### Where to find them?

- Where available, national case definitions issued by the MoH should be used. 


- All healthcare workers responsible for reporting cases to IBS must ensure that the patients meet these standardized case definitions.

- Case definitions can be based on clinical criteria, laboratory criteria or a combination of the two.


- Note
- - Case definitions are designed for surveillance purposes only. 
- - They are **not** used as diagnostic criteria for treatment and **do not** provide any indication of intention to treat.

<!-- .element: class="reference " -->

--

### Revising case definitions

- Case definitions that do not include a requirement for laboratory confirmation are generally more senstive. 

- The sensitivity and specificity of case definitions should be reviewed periodically


- Outbreak case definitions
- - During outbreaks, case definitions may need to be revised to incorporate additional elements of person, place, and time 
- - This is done to improve the specificity of the case definition and to include cases epidemiologically linked with the outbreak. 

<!-- .element: class="reference" -->

---

<!-- .slide: class="center" -->
<!-- .slide: data-background-color="#3784f4" -->
<!-- .slide: data-background-image="/assets/images/world-map-bg.svg" -->
<!-- .slide: data-background-position="left 40px top 40px" -->
<!-- .slide: data-background-size="52%" -->

# Step 4
<!-- .element style="text-align: center; margin-top: -150px;"-->

## Define alert thresholds

<!-- .element style="text-align: center; margin-top: -0px;"-->

--

### Alert thresholds

- Definition
- - An **alert threshold** is the critical number of cases used to trigger an alert. All alerts must be verified to determine if further risk assessment and response is required

<!-- .element: class="note" -->

- Definition
- - An **epidemic threshold** is the critical number of cases required for an epidemic to occur. It is used to confirm the emergence of an epidemic so as to step-up appropriate control measures. 

<!-- .element: class="note" -->

- Alongside a case definition, each disease or health condition prioritised in EWAR must be assigned an alert threshold (and, in some countries, an epidemic threshold).


--

### Types of alert thresholds

- Alert thresholds can broadly be categorised into two types:
- - 1\. **Fixed values** 
- -	2\. **Trends**

<!-- .element: class="reference" -->


- In emergencies, where prior baseline data may not be available, this prior trend may need to be based on empirical data and calculated using **moving averages** over a short period of time.

- In protracted emergencies, where longer **historical trends** are available, these trends can be calculated over longer time periods (e.g. months or years). 


--

### Example: fixed alert threshold 

![Meningitis fixed threshold](../assets/guidance/alert_fixed.png  "Meningitis fixed threshold")

--

### Example: moving average threshold 

![Malaria alert](/assets/guidance/alert_moving.png  "Malaria alert")

--

### Example: historical trend threshold 

![Dengue alert](/assets/guidance/alert_historical.png  "Dengue alert")

--

### Typical list of alert thresholds in emergencies

| Disease / Hazard | Alert threshold |
| -- | -- |
| 1\. Acute flaccid paralysis (suspected poliomyelitis) | 1 case |
| 2\. Acute hemorrhagic fever syndrome | 1 case |
| 3\. Acute jaundice syndrome (suspected hepatitis A/E)| &ge; 5 cases |
| 4\. Acute respiratory infection (suspected pneumonia)| x1.5 the baseline* |
| 5\. Acute watery diarrhea (suspected cholera)| x1.5 the baseline* |
| 6\. Bloody diarrhea (suspected dysentery)| &ge; 5 cases **or** x2 the baseline |
| 7\. Malaria (suspected or confirmed)| x1.5 the baseline* |
| 8\. Suspected measles| 1 case |
| 9\. Suspected meningitis| 1 case |

<!-- .element style="width: 90%;line-height: 10px;" -->

<span style="font-size: 14px; color: grey; margin-top: -60px">\* Baseline = average number of cases seen in the previous 3 weeks</span>

--

### Management of IBS alerts

- Once an alert has been triggered in IBS, it should be managed according to a predefined workflow and set of standard operating procedures. 

- These are discussed in more detail in the alert management module.


---

<!-- .slide: class="center" -->
<!-- .slide: data-background-color="#3784f4" -->
<!-- .slide: data-background-image="/assets/images/world-map-bg.svg" -->
<!-- .slide: data-background-position="left 40px top 40px" -->
<!-- .slide: data-background-size="52%" -->

# Step 5
<!-- .element style="text-align: center; margin-top: -150px;"-->

## Strengthen data collection and reporting
<!-- .element style="text-align: center; margin-top: -0px;"-->

--

### Zero reporting

- All reporting sites should be assessed for their ability to perform **zero reporting** (the mandatory reporting of 0 cases if none are seen). 

- **Zero reporting** avoids misinterpretation of the missing number, while also allowing the identification of non-responsive or "silent" health facilities. 

--

### Reporting of new cases

- **Only new (incident) cases should be reported**. If a patient returns to a health facility for a repeat visit for the same disease or health condition that has already been reported, they should _not_ be reported again in EWAR.

- If a case presents with two reportable health conditions (e.g. malaria and AWD) - _and if both are new diagnoses that have not previously been reported_ - then **both** new conditions should be reported.

--

### Reporting of total consultations

- Repeat consultations for diseases that have already been reported shoulld not be entered into EWAR. 
- However, the **total number of consultations** can be useful to act as a denominator in calculation of proportional morbidity.
- Therefore, it is recommended that this is included as the last row of the weekly form, to report the total number of consultations made during the week (both new and revisits).

--

### Paper-based tools

- Standard reporting tools should be provided to staff to ensure all data that is collected is correctly disaggregated and good quality. 
- Paper-based tools such as tally sheets are key to recording high volume of data collected via IBS. - These should be completed by electronic tools to support data entry and reporting, ideally at facility level.

--

### Additional considerations 

- **Double counting**. Health facilities should put in safeguards to prevent double counting of patients in a clinic that are referred between departments. 


- **Place of residence**. Especially for line listing of cases during outbreaks.

- **Cross border populations**. Consider information sharing needs.

--

### Frequency

- IBS data collected in EWAR in emergencies is always reported on a **weekly** basis. 

- Weekly IBS reports should follow an **epidemiological week** as defined by the Ministry of Health. This is commonly Monday to Sunday, but can vary from country to country.


- Note
- - If an outbreak is declared, then daily reporting of the specific disease by all facilities in the affected area is expected. 
- - This is achieved through daily line listing of case and deaths, and submission of the line lists by the health facilities on a daily basis.

<!-- .element: class="more-details" -->


--

### Supervision and feedback

- Data collection and reporting should be strengthened through regular monitoring and supervision, to motivate staff.

- Feedback should be provided to staff in health facilities to acknowledge receipt of their reports. Epidemiological bulletins should also be shared back with staff at health facility level, in order to provide feedback on system performance and to provide key analysis.


---

### Recap

- Indicator-based surveillance
- 1\. Agree on strategy
- 2\. Select priority diseases and other hazards
- 3\. Define case definitions
- 4\. Define alert thresholds
- 5\. Strengthen data collection and reporting

<!-- .element class="success" style="line-height: 40px"-->

---

### References

1. WHO Recommended Surveillance Standards. Second edition. Geneva, World Health Organization, 2002.

2. Early detection, assessment and response to acute public health events: implementation of 
early warning and response with a focus on event-based surveillance. Geneva, World Health Organization, 2014.



