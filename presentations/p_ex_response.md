
<!-- .slide: class="center ewars" -->
<!-- .slide: data-background-color="#37b2f4" -->
<!-- .slide: data-background-image="/assets/images/world-map-bg.svg" -->
<!-- .slide: data-background-position="left 40px top 40px" -->
<!-- .slide: data-background-size="52%" -->

# Practical exercise 
<!-- .element style="text-align: center; margin-top: -150px; color: #fff"-->

## Outbreak Response
<!-- .element style="text-align: center; margin-top: -0px; color: #fff"-->

---

<!-- .slide: class="ewars" -->

### Scenario

- Nambutu has been hit by severe flooding, which has affected all 6 provinces. 

- Around 1.8 million people have been displaced and over 1,000 are estimated to have been killed. 

- The government has made a request for international assistance. WHO is supporting the MoH to lead the health response and has declared the emergency as Grade 2, requiring Regional level support.

- A number of international and national NGOs are already on the ground and WHO is helping to coordinate their activities alongside MoH.

<!-- .element: style="float: left; width: 50%; margin-top:-10px"  -->

![Flood](../assets/training/scenario-flood1.jpg "Flood") 
![Flood](../assets/training/scenario-flood2.jpg "Flood") 

<!-- .element: style="float: right; width: 40%; margin-top:-40px" -->


--

<!-- .slide: class="ewars" -->

### Update

- It is 17 days since your EWAR system has detected an increase in acute watery diarrhoea cases. 

- You have conducted a risk assessment and assigned it **High risk**. 

- The laboratory samples that were taken during the risk assessment 2 weeks ago have just come back positive for _Vibrio cholerae O1_.

- You have conducted a joint outbreak investigation with MoH and NGOs and have identified 194 suspect cases among a population of 1,301 deemed to be at risk. 

<!-- .element: style="float: left; width: 50%; margin-top:-10px"  -->

![Cholera](../assets/training/scenario-cholera.jpg "Cholera") 
![Cholera](../assets/training/scenario-cholera2.jpg "Cholera") 

<!-- .element: style="float: right; width: 40%; margin-top:-40px" -->


--

![Map](../assets/training/scenario-nambutu.jpg "Map") 

<!-- .element: style="width: 80%; margin-top: -50px" -->


---

<!-- .slide: class="ewars" -->

### Exercise 1 

Website:	<a href="http://demo.ewars.ws" target="_blank">http://demo.ewars.ws</a>

|Group |Province|Username|Password|
|--|--|--|--|
|1|Aimal|demo.aimal@ewars.ws|ewars123|
|2|Rimpar|demo.rimpar@ewars.ws|ewars123|
|3|Elvoba|demo.elvoba@ewars.ws|ewars123|
|4|Jobrar|demo.jobrar@ewars.ws|ewars123|
|5|Dirran|demo.dirran@ewars.ws|ewars123|
|6|Laskuna|demo.laskuna@ewars.ws|ewars123|

<!-- .element: style="float: left; width: 60%; line-height: 10px" -->


- Task
- - Sign into the Demo EWARS account for your group.

<!-- .element class="reference" style="float: left; width: 100%; margin-top: 15px" -->

---

<!-- .slide: class="ewars" -->

### Exercise 2 

Following confirmation of cholera and declaration of the outbreak, a cholera line list has been created and deployed using EWARS-in-a-box.

- Task
- - Open the cholera line list and browse all the cases reported in your Province.

<!-- .element class="reference" style="float: left; width: 100%; margin-top: 15px" -->


---

<!-- .slide: class="ewars" -->

### Exercise 3

An NGO partner phones with the details of a new case, and asks you to enter it for them. No other information is currently known about the case.

- - Case Number: CHOL-356-NAM
- - **Name:** {Your name}
- - **Age:** 4 years
- - **Sex:** Male
- - **Location:** {Your province}
- - **Date of symptom onset:** 11 Feb 2019
- - **RDT result:** Positive
- - **Symptoms:** Acute rice watery diarrhoea, Vomiting, Abdominal cramps, Headache

<!-- .element class="case-study" style="line-height: 0px" -->

- Task
- - Submit a sample cholera case using the line list provided.

<!-- .element class="reference" style="float: left; width: 100%; margin-top: 15px" -->


---

<!-- .slide: class="ewars" -->

### Exercise 4

You've been asked to prepare some analysis to describe the outbreak by person, place and time.

- Task
- Create a notebook to show the following for your Province:
- - An epidemic curve to show the cases by week in the past 12 months
- - A chloropleth map to show the distribution of cases by Province (cumulative in the past 12 months)
- - Age and sex pie charts to show the distribution of cases by person (cumulative in the past 12 months)

<!-- .element class="reference" style="float: left; width: 100%; margin-top: 15px" -->


---

<!-- .slide: class="ewars" -->

### Exercise 5

Website:	<a href="http://demo.ewars.ws" target="_blank">http://demo.ewars.ws</a>

|Group |Province|Username|Password|
|--|--|--|--|
|1|Aimal|demo.admin.aimal@ewars.ws|ewars123|
|2|Rimpar|demo.admin.rimpar@ewars.ws|ewars123|
|3|Elvoba|demo.admin.elvoba@ewars.ws|ewars123|
|4|Jobrar|demo.admin.jobrar@ewars.ws|ewars123|
|5|Dirran|demo.admin.dirran@ewars.ws|ewars123|
|6|Laskuna|demo.admin.laskuna@ewars.ws|ewars123|

<!-- .element: style="float: left; width: 60%; line-height: 10px" -->

- Task
- - Sign out of your Geographic Administrator Account, and sign in again as the Account Administrator using the name of your Province.

<!-- .element class="reference" style="float: left; width: 100%; margin-top: 15px" -->

---

<!-- .slide: class="ewars" -->

### Exercise 6

As the outbreak evolves, the case definition has been revised and you've been asked to include dehydration status under signs and symptoms in the line list format.

- Task
- - In your new account admin role, go to Admin > Forms and **duplicate** the form called "[Demo] Cholera Line List"
- - Rename the form as "{Your province} Cholera Line List".
- - Under Signs and Symptoms > Add a new Select variables for Dehydration Status, with the options of "No dehydration, Some dehydration, Severe dehydration"

<!-- .element class="reference" style="float: left; width: 100%; margin-top: 15px" -->

---

<!-- .slide: class="ewars" -->

### Exercise 7

New NGO partners have arrived to run Cholera Treatment Centres based in the following health facilities. They all now require access to the newly updated Cholera Line List form.

|Group | Health Facility | Reporting User |
|--|--|--|
|Aimal|Bilnula PHCC| demo.bilnula@ewars.ws |
|Rimpar|Amgate PHCC | demo.amgate@ewars.ws |
|Elvoba|Heaford PHCC | demo.heaford@ewars.ws |
|Jobrar| Urum PHCC | demo.urum@ewars.ws |
|Dirran| Dirabi PHCC | demo.dirabi@ewars.ws |
| Laskuna|Ruypool PHCC|demo.ruypool@ewars.ws |

- Task
- - Go to Admin > Users and find the reporting user accounts.
- - Edit their Assignments and add permissions for "{Your province} Cholera Line List" so they can begin submitting cases right away! 


<!-- .element class="reference" style="float: left; width: 100%; margin-top: 15px" -->
