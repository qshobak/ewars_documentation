
<!-- .slide: class="center ewars" -->
<!-- .slide: data-background-color="#37b2f4" -->
<!-- .slide: data-background-image="/assets/images/world-map-bg.svg" -->
<!-- .slide: data-background-position="left 40px top 40px" -->
<!-- .slide: data-background-size="52%" -->


# Practical exercise
<!-- .element style="text-align: center; margin-top: -150px; color: #fff"-->

## Managing alerts
<!-- .element style="text-align: center; margin-top: -0px; color: #fff"-->


---

<!-- .slide: class="ewars" -->

### Scenario

- Nambutu has been hit by severe flooding, which has affected all 6 provinces. 

- Around 1.8 million people have been displaced and over 1,000 are estimated to have been killed. 

- The government has made a request for international assistance. WHO is supporting the MoH to lead the health response and has declared the emergency as Grade 2, requiring Regional level support.

- A number of international and national NGOs are already on the ground and WHO is helping to coordinate their activities alongside MoH.

<!-- .element: style="float: left; width: 50%; margin-top:-10px"  -->

![Flood](../assets/training/scenario-flood1.jpg "Flood") 
![Flood](../assets/training/scenario-flood2.jpg "Flood") 

<!-- .element: style="float: right; width: 40%; margin-top:-40px" -->


--

<!-- .slide: class="ewars" -->

### Priorities

- An urgent priority is to establish an Early Warning, Alert and Response System (EWARS), to detect and respond to potential outbreaks of epidemic-prone disease.

- A rapid risk assessment has been completed by WHO and MoH, and a total 6 diseases with epidemic potential have been identified and put under immediate surveillance. 

- A Weekly Indicator-based surveillance form has been designed and agreed in collaboration with the MoH. An EWARS in a box kit has been sent to Nambutu by the Regional office, and is now ready for implementation.

<!-- .element: style="float: left; width: 50%; margin-top:-10px"  -->

![EWARS-in-a-box](../assets/training/eiab.png "EWARS-in-a-box") 
![Heli](../assets/training/scenario-heli.png "Heli") 

<!-- .element: style="float: right; width: 40%; margin-top:-40px" -->



--

![Map](../assets/training/scenario-nambutu.jpg "Map") 

<!-- .element: style="width: 80%; margin-top: -50px" -->


---

<!-- .slide: class="ewars" -->

### Exercise 1 

Website:	<a href="http://demo.ewars.ws" target="_blank">http://demo.ewars.ws</a>

|Group |Province|Username|Password|
|--|--|--|--|
|1|Aimal|demo.aimal@ewars.ws|ewars123|
|2|Rimpar|demo.rimpar@ewars.ws|ewars123|
|3|Elvoba|demo.elvoba@ewars.ws|ewars123|
|4|Jobrar|demo.jobrar@ewars.ws|ewars123|
|5|Dirran|demo.dirran@ewars.ws|ewars123|
|6|Laskuna|demo.laskuna@ewars.ws|ewars123|

<!-- .element: style="float: left; width: 60%; line-height: 10px" -->


- Task
- - Sign into the Demo EWARS account for your Group.

<!-- .element class="reference" style="float: left; width: 100%; margin-top: 15px" -->

---

<!-- .slide: class="ewars" -->

### Exercise 2

- An NGO health coordinator has called and asked you to amend their weekly report for W9 2019. They have seen 14 cases and 1 death due to AWD among Under 5s.

|Group | Health Facility |
|--|--|
|Aimal|Bilnula PHCC|
|Rimpar|Amgate PHCC | 
|Elvoba|Heaford PHCC | 
|Jobrar| Urum PHCC | 
|Dirran| Dirabi PHCC | 
| Laskuna|Ruypool PHCC|

<!-- .element class="" style="width: 60%; font-size: 14px; line-height: 12px; margin-top: 20px;" -->

- Task
- - For the health facility in your Province, find the report and make the amendment based on this updated information.

<!-- .element class="reference" style="margin-top: 15px;" -->

---

<!-- .slide: class="ewars" -->

### Exercise 3

- You have been asked to present the Weekly Epidemiological Bulletin at the health cluster meeting. 

<!-- .element: style="float: left; width: 50%; margin-top:-10px"  -->

- Task
- - Download the latest Weekly Epidemiological Bulletin for W9 2019. 
- - Use the M&E Auditor to identify which health facilities did not report this week in your Province.

<!-- .element class="reference" style="margin-top: 45px; float: left; width 100%" -->

![Bulletin](../assets/training/scenario-bulletin.png "Bulletin") 

<!-- .element: style="float: right; width: 37%; margin-top:-80px" -->


---

<!-- .slide: class="ewars" -->

### Exercise 4

A colleague has just returned from a field visit with new information.

- EBS Report 1
- - 4 cases of acute watery diarrhoea with severe dehydration seen in a health facility (2 children, 2 adults) on the same day, all from the same community.

<!-- .element: class="case-study" -->

- Task
- - Submit an EBS Report based on the information you received.

<!-- .element class="reference" style="margin-top: 15px" -->

---

<!-- .slide: class="ewars" -->

### Exercise 5

You telephone the health facility and verify with the health staff that all cases reported correctly, and all met the case definition.


- Task
- - Open the alert that was generated in EWARS. View the steps involved in alert management. 
- - Update the verification information and take a decision on the next step.

<!-- .element class="reference" style="margin-top: 15px" -->


---

<!-- .slide: class="ewars" -->

### Exercise 6

You obtain the following information during your risk assessment.

<!-- .element: class="" -->

| **Components** | **Key information** | 
| --- | --- | 
| **Hazard assessment** |- AWD symptoms<br>- Laboratory confirmation samples taken but pending result |
| **Exposure assessment**|- Total of 24 cases and 7 deaths reported in 1 week.<br>- 1,301 people living in the affected community<br>- Mode of transmission unknown|
| **Context assessment** | - The event is occurring 8 km from the international border and there are also a high degree of movement between the community and neighbouring villages. <br>- The area is the poorest in the country and health infrastructure is limited.<br>- Many of the health care facilities charge a consultation fee and consequently the local population self-medicates during mild illness.<br>- There are strong beliefs that "strange diseases" are caused by sorcery.|

<!-- .element: class="" style="font-size: 14px; line-height: 18px;"-->

- Task
- - Enter this information into the Risk Assessment stage of the alert and move it to the next step.

<!-- .element class="reference" style="margin-top: 15px" -->


---

<!-- .slide: class="ewars" -->

### Exercise 7

You organise the information collected during your risk assessment into a likelihood and consequences table below.

|Likelihood of spread |Consequences if it were to occur|
|--|--|
|- The specific hazard and mode(s) of transmission have not been identified<br>- High reported CFR<br>- It is likely that some cases are not being detected. Therefore it is highly likely that further cases will occur if nothing is done|- Poor health-care system is poor and the ability to treat the cases is already limited<br>- Potential for unrest in communities because of cultural beliefs that sorcery is causing the deaths<br>- Event is in a border area and could affect the neighbouring country.<br>- New admissions will further stress acute care services and lead to worse clinical outcomes for hospitalized patients<br>- Negative economic and social impact of the cases and deaths in the affected communities|

<!-- .element: style="font-size: 14px; line-height: 18px;"-->

- Task
- - Based on the available information, assign a level of risk based on your assessment of the threat to public health posed by the event.

<!-- .element class="reference" style="margin-top: 15px" -->


---

<!-- .slide: class="ewars" -->

### Exercise 8

The District Medical Officer sends you an email.

- Email 
- To: Lead Epidemiologist
- From: District Medical Officer
- Importance : High
- - I am urgently requesting the results of the risk assessment. What was the result and what should the next steps be?

<!-- .element: class="case-study" -->

- Task
- - Assign an outcome to the alert.
- - What are the next steps you may consider taking inside EWARS?

<!-- .element: class="reference" -->

---

<!-- .slide: class="ewars" -->

### Exercise 9


- Task
- - When you have completed Exercises 3-6 on the web application, switch and repeat them using EWARS mobile on a phone. 

<!-- .element: class="reference" -->


